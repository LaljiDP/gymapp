import * as React from 'react'
import { Spinner } from 'react-bootstrap'
import './common.scss'

interface LoadingProps {
  className?: string
  centered?: boolean
}

const Loading: React.FC<LoadingProps> = (props): React.ReactElement => {
  return (
    <div className={props.className}>
      <div className={`loading ${props.centered && 'centered'}`}>
        <Spinner animation='border' variant='primary' />
      </div>
    </div>
  )
}

export default Loading
