import * as React from 'react'
import * as yup from 'yup'
import { Modal, Button, Form, FormControl, Spinner } from 'react-bootstrap'
import { useFormik } from 'formik'
import { useSelector } from 'react-redux'
import { StoreI } from '../../store/store.types'
import { toast } from 'react-toastify'
import { updateMemberData } from '../../store/actions/member.action'

interface TrainerModalPropsI {
  show: boolean
  onHide: () => void
  memberId: string
}

const trainerSchema = yup.object().shape({
  trainer: yup
    .string()
    .min(2)
    .required('Required field.'),
})

const TrainerModal: React.FC<TrainerModalPropsI> = (props): React.ReactElement => {
  const trainers = useSelector((state: StoreI) => state.users.trainers)

  const [loading, setLoading] = React.useState(false)

  const { handleChange, handleSubmit, values, touched, errors } = useFormik({
    initialValues: { trainer: '' },
    validationSchema: trainerSchema,
    onSubmit: values => {
      setLoading(true)
      updateMemberData(props.memberId, values, (err, msg) => {
        if (err) {
          toast.error(msg, { position: toast.POSITION.TOP_CENTER })
        } else {
          toast.success(msg, { position: toast.POSITION.TOP_CENTER })
        }
        setLoading(false)
        props.onHide()
      })
    },
  })

  return (
    <Modal centered={true} show={props.show} onHide={props.onHide}>
      <Modal.Header>Assign Trainer</Modal.Header>
      <Form onSubmit={handleSubmit}>
        <Modal.Body>
          <div className='p-2'>
            <FormControl
              name='trainer'
              isValid={touched.trainer && !errors.trainer}
              isInvalid={touched.trainer && errors.trainer ? true : false}
              onChange={handleChange}
              value={values.trainer}
              as='select'
            >
              <option>Select</option>
              {trainers.map(t => (
                <option key={t._id} value={t._id}>
                  {t.name}
                </option>
              ))}
            </FormControl>
          </div>
        </Modal.Body>
        <Modal.Footer>
          <Button size='sm' onClick={props.onHide} variant='secondary'>
            cancel
          </Button>
          <Button size='sm' variant='primary' type='submit'>
            Save {loading && <Spinner size='sm' animation='border' />}
          </Button>
        </Modal.Footer>
      </Form>
    </Modal>
  )
}

export default TrainerModal
