"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _express = _interopRequireDefault(require("express"));

var _passport = _interopRequireDefault(require("passport"));

var _jsonwebtoken = _interopRequireDefault(require("jsonwebtoken"));

var _bcryptjs = _interopRequireDefault(require("bcryptjs"));

var _users = _interopRequireDefault(require("../models/users.schema"));

var _users2 = require("../controllers/users.controller");

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { (0, _defineProperty2.default)(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

var router = _express.default.Router();
/* POST users listing. */


router.get('/info', _passport.default.authenticate('jwt', {
  session: false
}), function (req, res) {
  res.send(req.user);
});
router.post('/create',
/*#__PURE__*/
function () {
  var _ref = (0, _asyncToGenerator2.default)(
  /*#__PURE__*/
  _regenerator.default.mark(function _callee(req, res, next) {
    var _req$body, name, userName, email, password, mobileNo, dob, role, salt, encryptPass, user, token;

    return _regenerator.default.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _req$body = req.body, name = _req$body.name, userName = _req$body.userName, email = _req$body.email, password = _req$body.password, mobileNo = _req$body.mobileNo, dob = _req$body.dob, role = _req$body.role;
            _context.prev = 1;
            salt = _bcryptjs.default.genSaltSync(10);
            encryptPass = _bcryptjs.default.hashSync(password, salt);
            _context.next = 6;
            return new _users.default({
              name: name,
              userName: userName,
              email: email,
              password: encryptPass,
              mobileNo: mobileNo,
              dob: dob,
              role: role
            }).save();

          case 6:
            user = _context.sent;

            if (user) {
              token = _jsonwebtoken.default.sign({
                name: name,
                userName: userName,
                email: email,
                password: encryptPass
              }, process.env.PRIVATE_KEY);
              res.send({
                data: user,
                token: token
              });
            } else {
              res.status(501).json({
                message: 'Something wrong with creating user.'
              });
            }

            _context.next = 13;
            break;

          case 10:
            _context.prev = 10;
            _context.t0 = _context["catch"](1);
            res.status(501).json(_context.t0);

          case 13:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, null, [[1, 10]]);
  }));

  return function (_x, _x2, _x3) {
    return _ref.apply(this, arguments);
  };
}());
router.delete('/:id', _passport.default.authenticate('jwt', {
  session: false
}),
/*#__PURE__*/
function () {
  var _ref2 = (0, _asyncToGenerator2.default)(
  /*#__PURE__*/
  _regenerator.default.mark(function _callee2(req, res) {
    var id, user;
    return _regenerator.default.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            if (!req.user) res.status(401).json({
              message: 'Unauthorized'
            });
            id = req.params.id;
            _context2.prev = 2;
            _context2.next = 5;
            return _users.default.deleteOne({
              _id: id
            });

          case 5:
            user = _context2.sent;
            res.send(user);
            _context2.next = 12;
            break;

          case 9:
            _context2.prev = 9;
            _context2.t0 = _context2["catch"](2);
            res.status(501).json({
              message: 'Somthing wrong',
              trace: _context2.t0.toString()
            });

          case 12:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2, null, [[2, 9]]);
  }));

  return function (_x4, _x5) {
    return _ref2.apply(this, arguments);
  };
}());
router.get('/find', _passport.default.authenticate('jwt', {
  session: false
}),
/*#__PURE__*/
function () {
  var _ref3 = (0, _asyncToGenerator2.default)(
  /*#__PURE__*/
  _regenerator.default.mark(function _callee3(req, res) {
    var query, trainers;
    return _regenerator.default.wrap(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            if (!req.user) res.status(401).json({
              message: 'Unauthorized'
            });
            query = req.query;
            _context3.prev = 2;
            _context3.next = 5;
            return _users.default.find(_objectSpread({}, query));

          case 5:
            trainers = _context3.sent;

            if (trainers) {
              res.send(trainers);
            }

            _context3.next = 12;
            break;

          case 9:
            _context3.prev = 9;
            _context3.t0 = _context3["catch"](2);
            res.status(501).json({
              message: 'Error while fetching users',
              trace: _context3.t0.toString()
            });

          case 12:
          case "end":
            return _context3.stop();
        }
      }
    }, _callee3, null, [[2, 9]]);
  }));

  return function (_x6, _x7) {
    return _ref3.apply(this, arguments);
  };
}());
router.get('/total', _passport.default.authenticate('jwt', {
  session: false
}),
/*#__PURE__*/
function () {
  var _ref4 = (0, _asyncToGenerator2.default)(
  /*#__PURE__*/
  _regenerator.default.mark(function _callee4(req, res) {
    var total;
    return _regenerator.default.wrap(function _callee4$(_context4) {
      while (1) {
        switch (_context4.prev = _context4.next) {
          case 0:
            if (!req.user) res.status(401).json({
              message: 'Unauthorized'
            });
            _context4.prev = 1;
            _context4.next = 4;
            return _users.default.count();

          case 4:
            total = _context4.sent;
            res.send({
              total: total
            });
            _context4.next = 11;
            break;

          case 8:
            _context4.prev = 8;
            _context4.t0 = _context4["catch"](1);
            res.status(501).json({
              message: _context4.t0.toString()
            });

          case 11:
          case "end":
            return _context4.stop();
        }
      }
    }, _callee4, null, [[1, 8]]);
  }));

  return function (_x8, _x9) {
    return _ref4.apply(this, arguments);
  };
}());
router.get('/:page', _passport.default.authenticate('jwt', {
  session: false
}),
/*#__PURE__*/
function () {
  var _ref5 = (0, _asyncToGenerator2.default)(
  /*#__PURE__*/
  _regenerator.default.mark(function _callee5(req, res) {
    var page, users, totalRecord;
    return _regenerator.default.wrap(function _callee5$(_context5) {
      while (1) {
        switch (_context5.prev = _context5.next) {
          case 0:
            if (!req.user) res.status(401).json({
              message: 'Unauthorized'
            });
            page = req.params.page;
            console.log('page', page);
            if (!page) res.status(422).json({
              message: 'Bad Request ! parameter required'
            });
            _context5.prev = 4;
            _context5.next = 7;
            return _users.default.find({}, 'role _id name userName email dob lastLoggedIn createdAt updatedAt', {
              skip: page * 10,
              limit: 10
            }).sort({
              createdAt: 'desc'
            });

          case 7:
            users = _context5.sent;
            _context5.next = 10;
            return _users.default.count();

          case 10:
            totalRecord = _context5.sent;
            res.send({
              users: users,
              total: totalRecord
            });
            _context5.next = 17;
            break;

          case 14:
            _context5.prev = 14;
            _context5.t0 = _context5["catch"](4);
            res.status(501).json({
              message: 'Error while fetching members.',
              stacktrace: _context5.t0.toString()
            });

          case 17:
          case "end":
            return _context5.stop();
        }
      }
    }, _callee5, null, [[4, 14]]);
  }));

  return function (_x10, _x11) {
    return _ref5.apply(this, arguments);
  };
}());
var _default = router;
exports.default = _default;
//# sourceMappingURL=users.routes.js.map