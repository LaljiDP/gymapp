import * as React from 'react'
import { FormControl, Modal, Button, Form, Spinner } from 'react-bootstrap'
import { useFormik, FormikValues } from 'formik'
import * as yup from 'yup'
import { toast } from 'react-toastify'
import { createUser } from '../../store/actions/users.action'

const schema = yup.object().shape({
  name: yup.string().required(),
  userName: yup.string().required(),
  email: yup
    .string()
    .email('Incorrect email')
    .required(),
  mobileNo: yup.string().required(),
  dob: yup.string().required(),
  password: yup
    .string()
    .min(4, 'Minimum 4 character')
    .max(10, 'Maximum 10 character')
    .required(),
  cpass: yup
    .string()
    .required('Confirm password is required')
    .min(4)
    .oneOf([yup.ref('password'), null], 'Passwords must match'),
})

const initialValues = {
  name: '',
  userName: '',
  email: '',
  mobileNo: '',
  dob: '',
  password: '',
  cpass: '',
  role: 'Admin',
}

interface AddUserModalI {
  show: boolean
  onHide: () => void
}

const AddUserModal: React.FC<AddUserModalI> = (props): React.ReactElement => {
  const [loading, setLoading] = React.useState(false)

  const onSubmit = (values: FormikValues) => {
    setLoading(true)
    createUser(values, (err, msg) => {
      if (err) {
        toast.error(msg, { position: toast.POSITION.TOP_CENTER })
      } else {
        toast.success(msg, { position: toast.POSITION.TOP_CENTER })
      }
      props.onHide()
      setLoading(false)
    })
  }

  const { values, handleSubmit, handleReset, handleChange, touched, errors } = useFormik({
    initialValues,
    validationSchema: schema,
    onSubmit,
  })

  const onCloseModal = (e: any) => {
    handleReset(e)
    props.onHide()
  }

  return (
    <div>
      <Modal centered={true} show={props.show} onHide={props.onHide} size='xl'>
        <Modal.Header className='modal-header p-2 font-weight-bold'>
          <div className='modal-header-title'>Add New User</div>
        </Modal.Header>
        <Form noValidate={true} onSubmit={handleSubmit}>
          <Modal.Body>
            <div className=''>
              <Form.Control
                isInvalid={touched && errors.name ? true : false}
                isValid={touched.name && !errors.name}
                type='text'
                name='name'
                className='mt-2'
                value={values.name}
                placeholder='Name'
                onChange={handleChange}
              />
              <Form.Control
                isInvalid={touched && errors.userName ? true : false}
                isValid={touched.userName && !errors.userName}
                type='text'
                name='userName'
                className='mt-2'
                value={values.userName}
                placeholder='User name'
                onChange={handleChange}
              />
              <Form.Control
                isInvalid={touched && errors.email ? true : false}
                isValid={touched.email && !errors.email}
                type='email'
                name='email'
                className='mt-2'
                value={values.email}
                placeholder='Email'
                onChange={handleChange}
              />
              <Form.Control
                isInvalid={touched && errors.mobileNo ? true : false}
                isValid={touched.mobileNo && !errors.mobileNo}
                type='text'
                name='mobileNo'
                className='mt-2'
                value={values.mobileNo}
                placeholder='mobileNo'
                onChange={handleChange}
              />
              <div className='position-relative'>
                <div className='dob-label'>birth date</div>
                <FormControl
                  type='date'
                  name='dob'
                  placeholder='Date of birth'
                  value={values.dob}
                  isValid={touched.dob && !errors.dob}
                  isInvalid={touched && errors.dob ? true : false}
                  className='mt-2'
                  onChange={handleChange}
                />
              </div>
              <Form.Control
                isInvalid={touched && errors.password ? true : false}
                isValid={touched.password && !errors.password}
                type='password'
                name='password'
                className='mt-2'
                value={values.password}
                placeholder='Password'
                onChange={handleChange}
              />
              <Form.Control
                isInvalid={touched && errors.cpass && values.password !== values.cpass ? true : false}
                isValid={touched.cpass && !errors.cpass}
                type='password'
                name='cpass'
                className='mt-2'
                value={values.cpass}
                placeholder='Confirm Password'
                onChange={handleChange}
              />
              <FormControl as='select' name='role' value={values.role} onChange={handleChange} className='mt-2'>
                <option value='Trainer'> Trainer </option>
                <option value='Admin'> Admin </option>
              </FormControl>
            </div>
          </Modal.Body>
          <Modal.Footer>
            <Button onClick={onCloseModal} size='sm' variant='secondary'>
              Cancel
            </Button>
            <Button size='sm' disabled={loading} variant='success' type='submit'>
              Save {loading && <Spinner className='ml-2' size='sm' animation='border' />}
            </Button>
          </Modal.Footer>
        </Form>
      </Modal>
    </div>
  )
}

export default AddUserModal
