"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _mongoose = _interopRequireDefault(require("mongoose"));

var Schema = _mongoose.default.Schema;
var scheduleSchema = new Schema({
  schedule: [{
    day: String,
    excercise: String
  }],
  createdAt: {
    type: Date,
    default: Date.now
  },
  updatedAt: {
    type: Date,
    default: Date.now
  }
});

var _default = _mongoose.default.model('Schedule', scheduleSchema);

exports.default = _default;
//# sourceMappingURL=schedule.schema.js.map