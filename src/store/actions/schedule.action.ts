import request from '../../request'
import { dispatch } from '../index'
import { storeSchedule, loading, addSchedule } from '../reducers/schedule.reducer'
import { AxiosResponse } from 'axios'

export const fetchSchedule = async (page: number) => {
  try {
    dispatch(loading(true))
    const schedule: AxiosResponse = await request(`/api/schedule/${page}`)
    console.log('schedule', schedule)
    dispatch(storeSchedule(schedule.data))
    dispatch(loading(false))
  } catch (err) {
    dispatch(loading(false))
  }
}

export const createSchedule = async (payload: any, callback: (error: boolean, msg: string) => void) => {
  try {
    const res: AxiosResponse = await request.post('/api/schedule', payload)
    dispatch(addSchedule(res.data))
    callback(false, 'Schedule Added.')
  } catch (err) {
    callback(true, err.toString())
  }
}
