import request from '../../request'
import { dispatch } from '../index'
import { AxiosResponse } from 'axios'
import { storeUsers, loading, addUsersToStore, removeUser, storeTrainers } from '../reducers/users.reducer'
import { CallBackT } from '../store.types'

export const fetchUsers = async (page: number) => {
  try {
    dispatch(loading(true))
    const users: AxiosResponse = await request(`/api/user/${page}`)
    dispatch(storeUsers({ users: users.data.users, page, total: users.data.total }))
    dispatch(loading(false))
  } catch (err) {
    console.log('err', err)
  }
}

export const deleteUser = async (id: string, callback: (isDone: boolean, msg: string) => void) => {
  try {
    const users = await request.delete(`/api/user/${id}`)
    if (users) {
      dispatch(removeUser(id))
      callback(true, 'Removed.')
    }
  } catch (err) {
    console.log('err', err)
    callback(false, 'Something wrong!')
  }
}

export const fetchTrainers = async () => {
  try {
    const res = await request('/api/user/find?role=Trainer')
    dispatch(storeTrainers(res.data))
  } catch (err) {
    console.error(err)
  }
}

export const createUser = async (payload: any, callback: CallBackT) => {
  try {
    console.log('payload', payload)
    const user: AxiosResponse = await request.post('/api/user/create', payload)
    if (user) {
      dispatch(addUsersToStore(user.data.data))
      callback(false, 'User created')
    }
  } catch (err) {
    callback(true, err.toString())
  }
}
