"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _mongoose = _interopRequireDefault(require("mongoose"));

var Schema = _mongoose.default.Schema;
var userSchema = new Schema({
  name: String,
  dob: Date,
  userName: String,
  email: String,
  password: String,
  mobile_no: String,
  members: [{
    type: _mongoose.default.Schema.Types.ObjectId,
    ref: 'Member'
  }],
  lastLoggedIn: {
    type: Date,
    default: Date.now
  },
  createdAt: {
    type: Date,
    default: Date.now
  },
  updatedAt: {
    type: Date,
    default: Date.now
  },
  role: {
    type: String,
    required: true
  }
});

var _default = _mongoose.default.model('User', userSchema);

exports.default = _default;
//# sourceMappingURL=users.schema.js.map