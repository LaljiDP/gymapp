import request from '../../request'
import { storeMembers, loading, addMemberToStore, updateMember } from '../reducers/member.reducer'
import {
  addMembership,
  storeMembership,
  storeMemberInfo,
  updateMemberDetails,
  memberDetailloading,
} from '../reducers/memberDetails.reducer'
import { dispatch } from '../index'
import { AxiosResponse } from 'axios'
import { CallBackT } from '../store.types'

export const fetchMembers = async (page: number) => {
  try {
    dispatch(loading(true))
    const member = await request(`/api/member/${page}`)
    dispatch(storeMembers({ members: member.data.members, page, total: member.data.total }))
    dispatch(loading(false))
  } catch (err) {
    console.log('err', err)
  }
}

export const fetchMemberById = async (id: string) => {
  try {
    dispatch(memberDetailloading(true))
    const member = await request(`/api/member/${id}/single`)
    dispatch(storeMemberInfo(member.data))
    dispatch(memberDetailloading(false))
  } catch (err) {
    console.log('err', err)
    dispatch(memberDetailloading(false))
  }
}

export const addMember = async (payload: any, callback: (valid: boolean, msg: string) => void) => {
  try {
    const member: AxiosResponse = await request.post('/api/member', payload)

    if (member?.data?._id) {
      dispatch(addMemberToStore(member.data))
      callback(true, 'member added.')
    }
  } catch (err) {
    callback(false, 'Something wrong.')
  }
}

export const updateMemberData = async (id: string, payload: any, callback: (err: boolean, msg: string) => void) => {
  try {
    const member: AxiosResponse = await request.put(`/api/member/${id}`, payload)
    if (member.data) {
      dispatch(updateMemberDetails(member.data))
      dispatch(updateMember(member.data))
      console.log('member', member.data)
      callback(false, 'Member data Updated.')
    }
  } catch (err) {
    console.log('err', err)
    callback(true, err.toString())
  }
}

export const removeMember = (id: string, callback: CallBackT) => {
  try {
    const member = request.delete(`/api/member/${id}`)
    if (member) {
      callback(false, 'Member deleted')
    }
  } catch (err) {
    callback(true, err.toString())
  }
}

export const fetchMembershipByMember = async (userid: string, page: number, limit: number = 10) => {
  try {
    const res = await request(`/api/membership/${userid}/${page}/${limit}`)
    if (res.data) {
      dispatch(storeMembership(res.data))
    }
  } catch (err) {
    console.log('err', err.toString())
  }
}

export const createMembershipForMember = async (payload: any, callback: CallBackT) => {
  try {
    const res = await request.post(`/api/membership`, payload)
    dispatch(addMembership(res.data))
    callback(false, 'Membership updated.')
  } catch (err) {
    callback(true, err.toString())
  }
}
