import express from 'express'
import passport from 'passport'
import Membership from '../models/membership.schema'

const router = express.Router()

router.post('/', passport.authenticate('jwt', { session: false }), async (req, res) => {
  if (!req.user) res.status(401).json({ message: 'Unauthorized' })
  try {
    const membership = await new Membership({ ...req.body }).save()
    if (membership) {
      res.send(membership)
    } else {
      res.status(403).json({ message: 'Bad request !' })
    }
  } catch (err) {
    res.status(501).json({ message: 'Error while fetching Membership data', trace: err.toString() })
  }
})

router.get('/:userId/:page/:limit', passport.authenticate('jwt', { session: false }), async (req, res) => {
  if (!req.user) res.status(401).json({ message: 'Unauthorized' })

  const { userId, page, limit } = req.params
  console.log('req.params', req.params)
  try {
    if (userId) {
      const membership = await Membership.find({ user: userId }, null, {
        skip: Number(page) * Number(limit),
        limit: Number(limit),
      }).populate('user')
      if (membership) {
        res.send(membership)
      } else {
        res.send([])
      }
    } else {
      res.status(403).json({ message: 'Bad Request!' })
    }
  } catch (err) {
    res.status(501).json({ message: 'Error while fetching memberships data', trace: err.toString() })
  }
})

export default router
