import { UserI } from '../components/Users/users.types'

export interface StoreI {
  members: MemberReducerI
  users: UserReducerI
  dashboard: DashboardReducerI
  memberDetails: memberDetailsReducerI
  schedule: ScheduleReducerI
}

export interface memberDetailsReducerI {
  data: MemberI
  loading: boolean
  payments: any
  memberships: MemberShipI[]
}

export interface MemberShipI {
  _id: string
  plan: string
  startDate: string
  endDate: string
  months: number
  user?: UserI
  createdAt: string
  updatedAt: string
}

export interface MemberReducerI {
  data: MemberI[]
  loading: boolean
  page: number
  total: number
}

export interface ScheduleReducerI {
  data: ScheduleData[]
  loading: boolean
  page: number
  total: number
}

export interface DashboardReducerI {
  users: number
  loading: boolean
  members: number
}

export interface UserReducerI {
  data: UserI[]
  trainers: UserI[]
  loading: boolean
  page: number
  total: number
}

export interface ActionI {
  type: string
  payload: any
}

export interface ScheduleData {
  _id: string
  default?: []
  schedule: ScheduleT[]
  createdAt: string
  updatedAt: string
}

export type ScheduleT = {
  _id: string
  day: string
  excercise: string
}

export interface MemberI {
  _id: string
  name: string
  address: string
  city?: string
  state?: string
  blood_group?: string
  mobile_no: string
  height?: string
  trainer?: UserI
  weight?: number
  dob: string
  profile_pic?: string
  emergency_mobile_no: string
  schedule?: ScheduleI[]
  excercise: string
  dateOfJoining: string
  isDeleted?: boolean
  createdAt?: string
  updatedAt?: string
}

export interface ScheduleI {
  _id: string
  day: string
  excercise: string
}

export type CallBackT = (err: boolean, msg: string) => void
