import * as React from 'react'
import { useParams, useHistory } from 'react-router'
import { fetchMemberById, removeMember, fetchMembershipByMember } from '../../store/actions/member.action'
import { useSelector } from 'react-redux'
import { StoreI } from '../../store/store.types'
import Loading from '../Common/Loading'
import UserIcon from '../../assets/user.svg'
import { Button } from 'react-bootstrap'
import './member-detail.scss'
import MemberSchedule from './MemberSchedule'
import ScheduleModal from '../Modals/ScheduleModal'
import { toast } from 'react-toastify'
import MemberInfo from './MemberInfo'
import TrainerInfo from './TrainerInfo'
import ConfirmationModal from '../Modals/ConfirmationModal'
import TrainerModal from '../Modals/TrainerModal'
import { fetchTrainers } from '../../store/actions/users.action'
import Membership from './membership'
import AddMembershipModal from './membership/addMemberShip'

const MemberDetails: React.FC = (): React.ReactElement => {
  const { id = '' } = useParams()
  const [scheduleModal, setScheduleModal] = React.useState(false)
  const [trainerModal, setTrainerModal] = React.useState(false)
  const [membershipModal, setMembershipModal] = React.useState(false)
  const [deleteModal, setDeleteModal] = React.useState(false)
  const [collapse, setCollapse] = React.useState({
    schedule: false,
    trainer: false,
    membership: false,
  })

  const toggleMembershipModal = () => setMembershipModal(!membershipModal)

  const toggleTrainerModal = () => setTrainerModal(!trainerModal)

  const toggleDeleteModal = () => setDeleteModal(!deleteModal)

  const memberDetails = useSelector((state: StoreI) => state.memberDetails)

  const [data, setData] = React.useState(memberDetails.data)
  const [loading, setLoading] = React.useState(memberDetails.loading)
  const [loadingAction, setLoadingAction] = React.useState(false)

  React.useEffect(() => {
    setLoading(memberDetails.loading)
  }, [memberDetails.loading])

  React.useEffect(() => {
    setData(memberDetails.data)
  }, [memberDetails.data])

  const history = useHistory()

  const toggleScheduleModal = () => setScheduleModal(!scheduleModal)

  const handleCollapse = (data: any) => {
    setCollapse({
      ...collapse,
      ...data,
    })
  }

  React.useEffect(() => {
    fetchMemberById(id)
    fetchTrainers()
    fetchMembershipByMember(id, 0, 10)

    return () => {
      console.log('Unmounted <MemberDetailsInfo /> component.')
    }
  }, [id])

  const goBack = () => {
    history.goBack()
  }

  const handleDelete = () => {
    setLoadingAction(true)
    removeMember(data._id, (err, msg) => {
      if (err) {
        toast.error(msg, { position: toast.POSITION.TOP_CENTER })
      } else {
        toast.success(msg, { position: toast.POSITION.TOP_CENTER })
        history.goBack()
      }
      setLoadingAction(false)
    })
  }

  if (loading) {
    return <Loading centered={true} />
  }

  return (
    <>
      <div className='member-detail-container'>
        <div className='back header-sticky text-primary h5 pt-3 pl-3'>
          <i onClick={goBack} className='fa fa-chevron-left' />
          <div className='member-header p-3'>
            <div className='profile-pic'>
              <img src={UserIcon} height={100} width={100} alt='profile' />
            </div>
            <div className=''>
              <Button onClick={toggleDeleteModal} variant='danger'>
                <i className='fa fa-trash' />
                <span className='pl-2'>Delete</span>
              </Button>
            </div>
          </div>
        </div>
        <div className='p-2 member-body-container'>
          <div className='memeber-body p-3'>
            <MemberInfo data={data} />
          </div>

          <Membership
            toggleMembershipModal={toggleMembershipModal}
            data={memberDetails.memberships}
            expand={collapse.membership}
            handleCollapse={handleCollapse}
          />

          <MemberSchedule
            expand={collapse.schedule}
            handleCollapse={handleCollapse}
            data={data?.schedule}
            ispUdate={data?.schedule?.length ? true : false}
            openScheduleModal={toggleScheduleModal}
          />
          <TrainerInfo
            data={data?.trainer}
            expand={collapse.trainer}
            handleCollapse={handleCollapse}
            openTainerModal={toggleTrainerModal}
          />
        </div>
      </div>
      <ScheduleModal
        memberId={id}
        isUpdate={data?.schedule?.length ? true : false}
        update={true}
        scheduleData={data?.schedule}
        open={scheduleModal}
        onHide={toggleScheduleModal}
      />
      <TrainerModal memberId={id} show={trainerModal} onHide={toggleTrainerModal} />
      <ConfirmationModal
        show={deleteModal}
        onHide={toggleDeleteModal}
        onYesClick={handleDelete}
        loading={loadingAction}
      />
      <AddMembershipModal userId={id} show={membershipModal} onHide={toggleMembershipModal} />
    </>
  )
}

export default MemberDetails
