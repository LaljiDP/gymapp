import User from '../models/users.schema'

export const AddUser = (obj) => {
    return new User({...obj}).save()
}

