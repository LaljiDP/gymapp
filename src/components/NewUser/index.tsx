import * as React from 'react'
import { withRouter, RouteComponentProps } from 'react-router'
import { AxiosResponse, AxiosError } from 'axios'
import { useFormik, FormikValues } from 'formik'
import * as yup from 'yup'
import request from '../../request'
import { Form, Button, FormControl } from 'react-bootstrap'
import './styles.scss'
import { toast } from 'react-toastify'

const schema = yup.object().shape({
  name: yup.string().required(),
  userName: yup.string().required(),
  email: yup
    .string()
    .email()
    .required(),
  mobileNo: yup.string().required(),
  dob: yup.string().required(),
  password: yup
    .string()
    .min(4)
    .required(),
  cpass: yup
    .string()
    .min(4)
    .oneOf([yup.ref('password'), null], 'Passwords must match')
    .required(),
})

const initialValues = {
  name: '',
  userName: '',
  email: '',
  mobileNo: '',
  dob: '',
  password: '',
  cpass: '',
  role: 'Admin',
}

interface RegisterNewUserProps extends RouteComponentProps {
  isAdminPage: boolean
}

const RegisterNewUser: React.FC<RegisterNewUserProps> = (props): React.ReactElement => {
  const onSubmit = (values: FormikValues) => {
    const { name, userName, email, mobileNo, dob, password, cpass } = values
    if (!name || !userName || !email || !mobileNo || !dob || !password || !cpass) {
      toast.error('Please fill all information first.')
    } else if (password !== cpass) {
      toast.error('Password and confirm password not matched.')
    } else {
      request
        .post('/api/user/create', values)
        .then((res: AxiosResponse) => {
          console.log('res.data', res.data)
          if (res?.data?.token) {
            localStorage.setItem('token', res.data.token)
            props.history.push('/admin/users')
            toast.success('User Created', {
              position: toast.POSITION.TOP_CENTER,
            })
          }
        })
        .catch((err: AxiosError) => {
          console.log('err', err)
          toast.error('Something wrong with create user.', {
            position: toast.POSITION.TOP_CENTER,
          })
        })
    }
  }

  const { values, handleSubmit, handleChange, touched, errors } = useFormik({
    initialValues,
    validationSchema: schema,
    onSubmit,
  })

  return (
    <div className={`register ${!props.isAdminPage && 'p-3 vh-100 mt-5'}`}>
      {!props.isAdminPage && <div className='text-center alert-primary p-2 font-weight-bold'>New User?</div>}
      <div className='mt-5'>
        <Form noValidate={true} onSubmit={handleSubmit}>
          <Form.Control
            isInvalid={touched && errors.name ? true : false}
            isValid={touched.name && !errors.name}
            type='text'
            name='name'
            className='mt-2'
            value={values.name}
            placeholder='Name'
            onChange={handleChange}
          />
          <Form.Control
            isInvalid={touched && errors.userName ? true : false}
            isValid={touched.userName && !errors.userName}
            type='text'
            name='userName'
            className='mt-2'
            value={values.userName}
            placeholder='User name'
            onChange={handleChange}
          />
          <Form.Control
            isInvalid={touched && errors.email ? true : false}
            isValid={touched.email && !errors.email}
            type='email'
            name='email'
            className='mt-2'
            value={values.email}
            placeholder='Email'
            onChange={handleChange}
          />
          <Form.Control
            isInvalid={touched && errors.mobileNo ? true : false}
            isValid={touched.mobileNo && !errors.mobileNo}
            type='text'
            name='mobileNo'
            className='mt-2'
            value={values.mobileNo}
            placeholder='mobileNo'
            onChange={handleChange}
          />
          <Form.Control
            isInvalid={touched && errors.dob ? true : false}
            isValid={touched.dob && !errors.dob}
            type='date'
            name='dob'
            className='mt-2'
            value={values.dob}
            placeholder='DOB'
            onChange={handleChange}
          />
          <Form.Control
            isInvalid={touched && errors.password ? true : false}
            isValid={touched.password && !errors.password}
            type='password'
            name='password'
            className='mt-2'
            value={values.password}
            placeholder='Password'
            onChange={handleChange}
          />
          <Form.Control
            isInvalid={touched && errors.cpass && values.password !== values.cpass ? true : false}
            isValid={touched.cpass && !errors.cpass}
            type='password'
            name='cpass'
            className='mt-2'
            value={values.cpass}
            placeholder='Confirm Password'
            onChange={handleChange}
          />

          {props.isAdminPage && (
            <FormControl as='select' name='role' value={values.role} onChange={handleChange} className='mt-2'>
              <option value='Trainer'> Trainer </option>
              <option value='Admin'> Admin </option>
            </FormControl>
          )}

          <Button className='w-100 mt-4' type='submit'>
            Submit
          </Button>
        </Form>
      </div>
    </div>
  )
}

export default withRouter(RegisterNewUser)
