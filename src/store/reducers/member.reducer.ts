import { createSlice } from '@reduxjs/toolkit'
import { MemberReducerI, ActionI } from '../store.types'

const initialState: MemberReducerI = {
  data: [],
  loading: false,
  page: 0,
  total: 0,
}

export const memberSlice = createSlice({
  name: 'member',
  initialState: initialState,
  reducers: {
    storeMembers: (state, action: ActionI) => {
      state.data = action.payload.members
      state.total = action.payload.total
      state.page = action.payload.page
    },
    addMemberToStore: (state, action: ActionI) => {
      let members = state.data.slice()
      if (members.length === 10 && state.page === 0) {
        members.unshift(action.payload)
        members.pop()
        state.data = [...members]
        state.total = state.total + 1
      } else {
        members.unshift(action.payload)
        state.total = state.total + 1
      }
    },
    updateMember: (state, action: ActionI) => {
      const member = state.data.slice()
      const index = member.findIndex(o => o._id === action.payload._id)
      if (index >= 0) {
        member.splice(index, 1, action.payload)
      }
      state.data = member
    },
    loading: (state, action: any) => {
      state.loading = action.payload
    },
  },
})

export const { storeMembers, loading, updateMember, addMemberToStore } = memberSlice.actions

export const memberReducer = memberSlice.reducer
