import * as React from 'react'
import { Modal, Button, Spinner } from 'react-bootstrap'

interface ConfirmationModalI {
  show: boolean
  onHide: () => void
  onYesClick: () => void
  loading?: boolean
}

const ConfirmationModal: React.FC<ConfirmationModalI> = (props): React.ReactElement => {
  return (
    <Modal centered={true} show={props.show} onHide={props.onHide}>
      <Modal.Header className='p-1'>
        <div>
          <span className='h5 pl-2'>
            <i className='fa fa-trash' /> Delete
          </span>
        </div>
      </Modal.Header>
      <Modal.Body>Are you sure?</Modal.Body>
      <Modal.Footer>
        <Button size='sm' onClick={props.onHide} variant='secondary'>
          No
        </Button>
        <Button size='sm' onClick={props.onYesClick} variant='primary'>
          Yes
          {props.loading && <Spinner animation='border' className='ml-2' size='sm' />}
        </Button>
      </Modal.Footer>
    </Modal>
  )
}

export default ConfirmationModal
