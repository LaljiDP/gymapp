"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _express = _interopRequireDefault(require("express"));

var _path = _interopRequireDefault(require("path"));

var _dotenv = _interopRequireDefault(require("dotenv"));

var _passport = _interopRequireDefault(require("passport"));

var _cors = _interopRequireDefault(require("cors"));

var _cookieParser = _interopRequireDefault(require("cookie-parser"));

var _morgan = _interopRequireDefault(require("morgan"));

var _auth = _interopRequireDefault(require("./routes/auth.routes"));

var _users = _interopRequireDefault(require("./routes/users.routes"));

var _members = _interopRequireDefault(require("./routes/members.route"));

var _schedule = _interopRequireDefault(require("./routes/schedule.routes"));

var _settings = _interopRequireDefault(require("./routes/settings.routes"));

var _membership = _interopRequireDefault(require("./routes/membership.route"));

var _index = _interopRequireDefault(require("./routes/index"));

var _mongoose = _interopRequireDefault(require("mongoose"));

var _users2 = _interopRequireDefault(require("./models/users.schema"));

_dotenv.default.config();

require('./authentication/passport');

var app = (0, _express.default)();
app.use((0, _morgan.default)('dev'));
app.use((0, _cors.default)());
app.use(_express.default.json());
app.use(_express.default.urlencoded({
  extended: false
}));
app.use((0, _cookieParser.default)());

_mongoose.default.connect(process.env.MONGO_URL, {
  useNewUrlParser: true
}).then(function () {
  return console.log('MONGODB CONNECTED.');
}).catch(function (err) {
  return console.log('err', err);
});

app.use('/api/', _index.default);
app.use('/api/user', _users.default);
app.use('/api/auth', _auth.default);
app.use('/api/member', _members.default);
app.use('/api/membership', _membership.default);
app.use('/api/schedule', _schedule.default);
app.use('/api/setting', _settings.default);
app.use(_express.default.static(_path.default.join(__dirname, '../build')));
app.get('*', function (req, res) {
  res.sendFile(_path.default.join(__dirname, '../build/index.html'));
});
var _default = app;
exports.default = _default;
//# sourceMappingURL=app.js.map