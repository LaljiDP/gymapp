import React from 'react'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import NotFound404 from './components/Common/NotFound404'
import Login from './components/Login'
import 'bootstrap/dist/css/bootstrap.min.css'
import RegisterNewUser from './components/NewUser'
import AdminRoutes from './components/Routes/AdminRoutes'
import PrivateRoutes from './components/Routes/PrivateRoutes'
import Logout from './components/Common/Logout'
import { ToastContainer } from 'react-toastify'

const App: React.FC = () => {
  return (
    <div className='App'>
      <Router>
        <Switch>
          <Route path='/' exact={true} component={Login} />
          <Route path='/register' exact={true} component={RegisterNewUser} />
          <Route path='/logout' exact={true} component={Logout} />
          <PrivateRoutes path='/admin' component={AdminRoutes} />
          <Route component={NotFound404} />
        </Switch>
      </Router>
      <ToastContainer />
    </div>
  )
}

export default App
