"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _passport = _interopRequireDefault(require("passport"));

var JwtStrategy = require('passport-jwt').Strategy,
    ExtractJwt = require('passport-jwt').ExtractJwt;

var opts = {};
opts.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
opts.secretOrKey = process.env.PRIVATE_KEY;

_passport.default.use(new JwtStrategy(opts, function (jwt_payload, done) {
  if (jwt_payload) {
    return done(null, jwt_payload);
  } else {
    return done(err, false);
  }
}));
//# sourceMappingURL=passport.js.map