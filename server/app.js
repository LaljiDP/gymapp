import express from 'express'
import path from 'path'
import dotenv from 'dotenv'
import passport from 'passport'
import cors from 'cors'
import cookieParser from 'cookie-parser'
import logger from 'morgan'
import authRoutes from './routes/auth.routes'
import userRoutes from './routes/users.routes'
import memberRoute from './routes/members.route'
import scheduleRoute from './routes/schedule.routes'
import settingRoute from './routes/settings.routes'
import membershipRoute from './routes/membership.route'
import indexRouter from './routes/index'
import mongoose from 'mongoose'
import User from './models/users.schema'

dotenv.config()
require('./authentication/passport')
const app = express()
app.use(logger('dev'))
app.use(cors())
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(cookieParser())

mongoose
  .connect(process.env.MONGO_URL, { useNewUrlParser: true })
  .then(() => console.log('MONGODB CONNECTED.'))
  .catch(err => console.log('err', err))

app.use('/api/', indexRouter)

app.use('/api/user', userRoutes)

app.use('/api/auth', authRoutes)

app.use('/api/member', memberRoute)

app.use('/api/membership', membershipRoute)

app.use('/api/schedule', scheduleRoute)

app.use('/api/setting', settingRoute)

app.use(express.static(path.join(__dirname, '../build')))

app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname, '../build/index.html'))
})

export default app
