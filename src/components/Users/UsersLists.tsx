import * as React from 'react'
import { UserI } from './users.types'
import UserCard from '../Common/UserCard'

interface UsersListsPropsI {
  data: UserI[]
  loading: boolean
  page: number
  handleDelete: (id: string) => void
}

const UsersLists: React.FC<UsersListsPropsI> = (props): React.ReactElement => {
  return (
    <div className='users-lists '>
      {props.data?.map(user => (
        <UserCard user={user} key={user._id} action={true} onDelete={props.handleDelete} />
      ))}
    </div>
  )
}

export default UsersLists
