import * as React from 'react'
import UserCard from '../Common/UserCard'
import { useHistory } from 'react-router'
import { MemberI } from '../../store/store.types'

interface MembersListPropsI {
  data: MemberI[]
  loading: boolean
  page: number
}

const MembersList: React.FC<MembersListPropsI> = (props): React.ReactElement => {
  const { data = [] } = props
  const history = useHistory()

  const handleCardClick = (id: string) => {
    history.push(`/admin/members/${id}`)
  }

  return (
    <div className='member-lists mt-4'>
      {props?.data?.map(member => (
        <UserCard key={member._id} onClick={handleCardClick} user={member} />
      ))}
      {data?.length === 0 && <div>No Members Found</div>}
    </div>
  )
}

export default MembersList
