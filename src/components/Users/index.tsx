import * as React from 'react'
import { fetchUsers, deleteUser } from '../../store/actions/users.action'
import { useSelector } from 'react-redux'
import { StoreI } from '../../store/store.types'
import UsersLists from './UsersLists'
import './users.scss'
import TogglePlusButton from '../Common/TogglePlusButton'
import { FormControl } from 'react-bootstrap'
import Pagination from '../Common/Pagination'
import AddUserModal from '../Modals/AddUserModal'
import Loading from '../Common/Loading'
import ConfirmationModal from '../Modals/ConfirmationModal'
import { toast } from 'react-toastify'

const Users: React.FC = (): React.ReactElement => {
  const users = useSelector((state: StoreI) => state.users)

  const [userModal, setUserModal] = React.useState(false)

  const [loading, setLoading] = React.useState(false)

  const [deleteId, setDeleteId] = React.useState('')

  const [deleteModal, toggleDeleteModal] = React.useState(false)

  const totalPage = Math.ceil(users.total / 10)

  const [hasNext, setHasNext] = React.useState(totalPage > users.page + 1)

  const [hasPrev, setHasPrev] = React.useState(users.page >= 1)

  React.useEffect(() => {
    fetchUsers(0)
  }, [])

  React.useEffect(() => {
    setHasNext(totalPage > users.page + 1)
  }, [users.page, users.total, totalPage])

  React.useEffect(() => {
    setHasPrev(users.page >= 1)
  }, [hasNext, users.page])

  const handleNextPageChange = () => {
    if (hasNext) {
      fetchUsers(users.page + 1)
    }
  }

  const handlePrevPageChange = () => {
    if (hasPrev) {
      fetchUsers(users.page - 1)
    }
  }

  const openDeleteModal = (id: string) => {
    setDeleteId(id)
    toggleDeleteModal(!deleteModal)
  }

  const hideModal = () => {
    setDeleteId('')
    toggleDeleteModal(!deleteModal)
  }

  const handleDelete = () => {
    setLoading(true)
    deleteUser(deleteId, (isDone, msg) => {
      if (isDone) {
        toast.success(msg, {
          position: toast.POSITION.TOP_CENTER,
        })
        setLoading(false)
      } else {
        toast.error(msg, {
          position: toast.POSITION.TOP_CENTER,
        })
        setLoading(false)
      }
      hideModal()
    })
  }

  const toggleUserModal = () => {
    setUserModal(!userModal)
  }

  return (
    <div className='users-container p-2'>
      <TogglePlusButton actionFC={toggleUserModal} />
      <FormControl type='search' name='search' size='sm' placeholder='search users...' />
      <Pagination
        handleNextPageChange={handleNextPageChange}
        handlePrevPageChange={handlePrevPageChange}
        hasNext={hasNext}
        hasPrev={hasPrev}
        currentPage={users.page + 1}
        totalPage={totalPage}
      />
      {users.loading ? (
        <Loading className='mt-5' />
      ) : (
        <UsersLists handleDelete={openDeleteModal} data={users.data} loading={users.loading} page={users.page} />
      )}
      <AddUserModal show={userModal} onHide={toggleUserModal} />
      <ConfirmationModal loading={loading} show={deleteModal} onYesClick={handleDelete} onHide={hideModal} />
    </div>
  )
}

export default Users
