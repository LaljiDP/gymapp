import express from 'express'
import passport from 'passport'
import Setting from '../models/setting.schema'

const router = express.Router()

router.get('/', passport.authenticate('jwt', { session: false }), async (req, res) => {
  if (!req.user) res.status(401).json({ message: 'Unauthorized' })
  try {
    const settings = await Setting.find()
    res.send(settings)
  } catch (err) {
    res.status(501).json({ message: 'Error while fething settings.', trace: err.toString() })
  }
})

router.post('/', passport.authenticate('jwt', { session: false }), async (req, res) => {
  if (!req.user) res.status(401).json({ message: 'Unauthorized' })
  const { payload } = req.body
  try {
    for (let p of payload) {
      const ss = await new Setting({ ...p }).save()
      console.log('ss', ss)
    }
    res.send({ saved: true })
  } catch (err) {
    res.status(501).json({ message: 'Error while storing settings', trace: err.toString() })
  }
})

export default router
