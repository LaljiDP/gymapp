import * as React from 'react'
import { Transition } from 'react-transition-group'
import { MemberShipI } from '../../../store/store.types'
import moment from 'moment'

interface MembershipPropsI {
  handleCollapse: (data: any) => void
  expand: boolean
  data: MemberShipI[]
  toggleMembershipModal: () => void
}

const Membership: React.FC<MembershipPropsI> = ({
  handleCollapse,
  expand,
  data,
  toggleMembershipModal,
}): React.ReactElement => {
  return (
    <div className='membership'>
      <div className='alert font-weight-bold alert-primary'>
        <div className='d-flex justify-content-between'>
          <div>Membership Details</div>
          <div className='schedule-icons'>
            <i onClick={toggleMembershipModal} className='fa fa-plus' />
            <i
              onClick={() => handleCollapse({ membership: !expand })}
              className={`fa ${expand ? 'fa-angle-down' : 'fa-angle-right'} pl-4`}
            />
          </div>
        </div>
      </div>
      <Transition in={expand} timeout={1000}>
        {state => (
          <div className={`schedule-container ${expand ? 'd-block' : 'd-none'}`}>
            <div className='p-3'>
              {data?.length === 0 && <div> No Records found. </div>}
              {data?.map((s: any) => (
                <React.Fragment key={s._id}>
                  <div className='d-flex justify-content-between'>
                    <div>
                      <div>Plan</div>
                      <div>Month</div>
                      <div>Start date</div>
                      <div>End date</div>
                      <div>Amount</div>
                    </div>
                    <div className='pl-3 pr-3'>
                      <div className='day text-capitalize'>{s.plan}</div>
                      <div className=''>{s?.months}</div>
                      <div className=''>{moment(s?.startDate).format('ll')}</div>
                      <div className=''>{moment(s?.endDate).format('ll')}</div>
                      <div className=''>
                        <i className='fa fa-inr' /> {s?.amount}
                      </div>
                    </div>
                  </div>
                  <hr />
                </React.Fragment>
              ))}
            </div>
          </div>
        )}
      </Transition>
    </div>
  )
}

export default Membership
