import { combineReducers } from '@reduxjs/toolkit'
import { memberReducer } from './member.reducer'
import { userReducer } from './users.reducer'
import { dashboardReducer } from './dashboard.reducer'
import { memberDetailsReducer } from './memberDetails.reducer'
import { scheduleReducer } from './schedule.reducer'

export const rootReducer = combineReducers({
  members: memberReducer,
  users: userReducer,
  dashboard: dashboardReducer,
  memberDetails: memberDetailsReducer,
  schedule: scheduleReducer,
})

export type RootState = ReturnType<typeof rootReducer>

export default rootReducer
