"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _mongoose = _interopRequireDefault(require("mongoose"));

var Schema = _mongoose.default.Schema;
var memberSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  dob: {
    type: String,
    required: true
  },
  address: {
    type: String,
    required: true
  },
  weight: Number,
  height: String,
  email: String,
  blood_group: String,
  city: String,
  state: String,
  profile_pic: String,
  country: String,
  mobile_no: String,
  emergency_mobile_no: String,
  schedule: [{
    day: String,
    excercise: String,
    default: []
  }],
  trainer: {
    type: _mongoose.default.Schema.Types.ObjectId,
    ref: 'User'
  },
  memberships: [{
    type: _mongoose.default.Schema.Types.ObjectId,
    ref: 'Membership'
  }],
  excercise: {
    type: String,
    required: true
  },
  dateOfJoining: {
    type: Date,
    required: true,
    default: Date.now
  },
  isDeleted: {
    tpye: Boolean,
    default: false
  },
  createdAt: {
    type: Date,
    default: Date.now
  },
  updatedAt: {
    type: Date,
    default: Date.now
  }
});

var _default = _mongoose.default.model('Member', memberSchema);

exports.default = _default;
//# sourceMappingURL=members.schema.js.map