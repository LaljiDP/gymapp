import express from 'express'
import bcrypt from 'bcryptjs'
import jwt from 'jsonwebtoken'
import User from '../models/users.schema'
const router = express.Router()

/* GET users listing. */
router.post('/login', async (req, res) => {
  const { username, password } = req.body
  
  const user = await User.findOne({ userName: username })
  console.log('user', user)
  if(user) {
    const isPasswordCorrect = bcrypt.compareSync(password, user.password) 
    if(isPasswordCorrect) {
      const token = jwt.sign({ user }, process.env.PRIVATE_KEY)
      res.send({ token })
    } else {
      res.status(401).json({ message: 'Invalid credentials' })
    }
  } else {
    res.status(401).json({ message: 'Unauthorized' })
  }
})

export default router
