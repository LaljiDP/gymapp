"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _express = _interopRequireDefault(require("express"));

var _schedule = _interopRequireDefault(require("../models/schedule.schema"));

var _passport = _interopRequireDefault(require("passport"));

var router = _express.default.Router();

router.get('/:page', _passport.default.authenticate('jwt', {
  session: false
}),
/*#__PURE__*/
function () {
  var _ref = (0, _asyncToGenerator2.default)(
  /*#__PURE__*/
  _regenerator.default.mark(function _callee(req, res) {
    var page, schedule;
    return _regenerator.default.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            if (!req.user) res.status(401).json({
              message: 'Unatuhorized'
            });
            page = req.params.page;
            _context.prev = 2;
            _context.next = 5;
            return _schedule.default.find({}, null, {
              skip: page * 10,
              limit: 10
            });

          case 5:
            schedule = _context.sent;

            if (schedule) {
              res.send(schedule);
            }

            _context.next = 12;
            break;

          case 9:
            _context.prev = 9;
            _context.t0 = _context["catch"](2);
            res.status(501).json({
              message: 'Error while fetching Schedule',
              trace: _context.t0.toString()
            });

          case 12:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, null, [[2, 9]]);
  }));

  return function (_x, _x2) {
    return _ref.apply(this, arguments);
  };
}());
router.post('/', _passport.default.authenticate('jwt', {
  session: false
}),
/*#__PURE__*/
function () {
  var _ref2 = (0, _asyncToGenerator2.default)(
  /*#__PURE__*/
  _regenerator.default.mark(function _callee2(req, res) {
    var schedule, schedle;
    return _regenerator.default.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            if (!req.user) res.status(401).json({
              message: 'Unatuhorized'
            });
            schedule = req.body.schedule;
            _context2.prev = 2;
            _context2.next = 5;
            return new _schedule.default({
              schedule: schedule
            }).save();

          case 5:
            schedle = _context2.sent;

            if (schedle) {
              res.send(schedle);
            }

            _context2.next = 12;
            break;

          case 9:
            _context2.prev = 9;
            _context2.t0 = _context2["catch"](2);
            res.status(501).json({
              message: 'Error while saving schedule.',
              trace: _context2.t0.toString()
            });

          case 12:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2, null, [[2, 9]]);
  }));

  return function (_x3, _x4) {
    return _ref2.apply(this, arguments);
  };
}());
var _default = router;
exports.default = _default;
//# sourceMappingURL=schedule.routes.js.map