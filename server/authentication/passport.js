import passport from 'passport'

var JwtStrategy = require('passport-jwt').Strategy,
    ExtractJwt = require('passport-jwt').ExtractJwt;
var opts = {}
opts.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
opts.secretOrKey = process.env.PRIVATE_KEY;

passport.use(new JwtStrategy(opts, function(jwt_payload, done) {
    if(jwt_payload) {
        return done(null, jwt_payload)
    } else {
        return done(err, false)
    }    
}));
