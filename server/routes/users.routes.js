import express from 'express'
import passport from 'passport'
import jwt from 'jsonwebtoken'
import bcrypt from 'bcryptjs'
import User from '../models/users.schema'
import { AddUser } from '../controllers/users.controller'
const router = express.Router()

/* POST users listing. */

router.get('/info', passport.authenticate('jwt', { session: false }), (req, res) => {
  res.send(req.user)
})

router.post('/create', async (req, res, next) => {
  const { name, userName, email, password, mobileNo, dob, role } = req.body
  try {
    const salt = bcrypt.genSaltSync(10)
    const encryptPass = bcrypt.hashSync(password, salt)
    const user = await new User({
      name,
      userName,
      email,
      password: encryptPass,
      mobileNo,
      dob,
      role,
    }).save()

    if (user) {
      var token = jwt.sign(
        {
          name,
          userName,
          email,
          password: encryptPass,
        },
        process.env.PRIVATE_KEY
      )
      res.send({ data: user, token })
    } else {
      res.status(501).json({ message: 'Something wrong with creating user.' })
    }
  } catch (err) {
    res.status(501).json(err)
  }
})

router.delete('/:id', passport.authenticate('jwt', { session: false }), async (req, res) => {
  if (!req.user) res.status(401).json({ message: 'Unauthorized' })
  const { id } = req.params
  try {
    const user = await User.deleteOne({ _id: id })
    res.send(user)
  } catch (err) {
    res.status(501).json({ message: 'Somthing wrong', trace: err.toString() })
  }
})

router.get('/find', passport.authenticate('jwt', { session: false }), async (req, res) => {
  if (!req.user) res.status(401).json({ message: 'Unauthorized' })
  const { query } = req
  try {
    const trainers = await User.find({ ...query })
    if (trainers) {
      res.send(trainers)
    }
  } catch (err) {
    res.status(501).json({ message: 'Error while fetching users', trace: err.toString() })
  }
})

router.get('/total', passport.authenticate('jwt', { session: false }), async (req, res) => {
  if (!req.user) res.status(401).json({ message: 'Unauthorized' })
  try {
    const total = await User.count()
    res.send({ total })
  } catch (err) {
    res.status(501).json({ message: err.toString() })
  }
})

router.get('/:page', passport.authenticate('jwt', { session: false }), async (req, res) => {
  if (!req.user) res.status(401).json({ message: 'Unauthorized' })
  const { page } = req.params

  console.log('page', page)

  if (!page) res.status(422).json({ message: 'Bad Request ! parameter required' })
  try {
    const users = await User.find({}, 'role _id name userName email dob lastLoggedIn createdAt updatedAt', {
      skip: page * 10,
      limit: 10,
    }).sort({
      createdAt: 'desc',
    })
    const totalRecord = await User.count()
    res.send({ users, total: totalRecord })
  } catch (err) {
    res.status(501).json({ message: 'Error while fetching members.', stacktrace: err.toString() })
  }
})

export default router
