import * as React from 'react'
import UserIcon from '../../assets/user.svg'
import './common.scss'
import { UserI } from '../Users/users.types'
import { MemberI } from '../../store/store.types'

interface UserCardPropsI {
  user: MemberI | UserI
  action?: boolean
  onClick?: (id: string) => void
  onDelete?: (id: string) => void
}

const UserCard: React.FC<UserCardPropsI> = (props): React.ReactElement => {
  const { user, action, onDelete = () => {}, onClick = () => {} } = props
  return (
    <div onClick={() => onClick(user._id)} className='user-card p-2 mt-2' key={user._id}>
      <div className='d-flex'>
        <div className='profile'>
          <img className='mr-3' src={user.profile_pic || UserIcon} alt='profile' />
        </div>
        <div>{user.name} </div>
      </div>
      <div>
        {action && (
          <div className='icons'>
            <i onClick={() => onDelete(user._id)} className='fa fa-trash text-danger'/>
          </div>
        )}
      </div>
    </div>
  )
}

export default UserCard
