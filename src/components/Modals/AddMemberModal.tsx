import * as React from 'react'
import { useFormik, FormikValues } from 'formik'
import { toast } from 'react-toastify'
import * as yup from 'yup'
import { Modal, FormControl, Button, Form, Spinner } from 'react-bootstrap'
import './modals.scss'
import { addMember } from '../../store/actions/member.action'

interface AddMemberModalPropsI {
  show: boolean
  onHide: () => void
}

const initialPayload = {
  name: '',
  email: '',
  address: '',
  blood_group: '',
  height: '',
  weight: '',
  dob: '',
  dateOfJoining: '',
  emergency_mobile_no: '',
  city: '',
  state: '',
  country: '',
  excercise: '',
  profile_pic: '',
}

const schema = yup.object().shape({
  name: yup.string().required(),
  address: yup.string().required(),
  dob: yup.string().required(),
  dateOfJoining: yup.string().required(),
  excercise: yup.string().required(),
})

const AddMemberModal: React.FC<AddMemberModalPropsI> = (props): React.ReactElement => {
  const [loading, setLoading] = React.useState(false)

  const handleSubmitForm = (values: FormikValues) => {
    setLoading(true)
    addMember(values, (isvalid, msg) => {
      if (isvalid) {
        toast.success(msg, { position: toast.POSITION.TOP_CENTER })
        setLoading(false)
        props.onHide()
      } else {
        toast.error(msg, { position: toast.POSITION.TOP_CENTER })
        setLoading(false)
      }
    })
  }

  const { handleSubmit, handleChange, handleReset, values, errors, touched } = useFormik({
    initialValues: initialPayload,
    validationSchema: schema,
    onSubmit: handleSubmitForm,
  })

  return (
    <div>
      <Modal show={props.show} onHide={props.onHide} size='xl'>
        <Modal.Header className='modal-header p-2 font-weight-bold'>
          <div className='modal-header-title'>Add New Member</div>
        </Modal.Header>
        <Form noValidate={true} onSubmit={handleSubmit}>
          <Modal.Body>
            <div className='modal-header-form'>
              <div className='control'>
                <FormControl
                  type='text'
                  name='name'
                  onChange={handleChange}
                  value={values.name}
                  size='sm'
                  isInvalid={touched.name && errors.name ? true : false}
                  isValid={touched.name && !errors.name}
                  placeholder='Name'
                />
              </div>
              <div className='pt-2'>
                <FormControl
                  isValid={false}
                  className=''
                  type='email'
                  name='email'
                  onChange={handleChange}
                  size='sm'
                  placeholder='Email'
                />
              </div>
              <div className='pt-2'>
                <FormControl
                  as='textarea'
                  isInvalid={touched.address && errors.address ? true : false}
                  isValid={touched.address && !errors.address}
                  className=''
                  value={values.address}
                  type='textarea'
                  onChange={handleChange}
                  name='address'
                  size='sm'
                  placeholder='Address'
                />
              </div>

              <div className='pt-2'>
                <FormControl
                  value={values.height}
                  className=''
                  type='text'
                  onChange={handleChange}
                  name='height'
                  size='sm'
                  placeholder={`Height (5"2)`}
                />
              </div>

              <div className='pt-2'>
                <FormControl
                  className=''
                  type='text'
                  onChange={handleChange}
                  name='weight'
                  size='sm'
                  placeholder={`Weight (in KG)`}
                />
              </div>
              <div className='pt-2'>
                <FormControl
                  className=''
                  type='text'
                  value={values.blood_group}
                  onChange={handleChange}
                  name='blood_group'
                  size='sm'
                  placeholder={`Blood group (A+, B+, AB+)`}
                />
              </div>

              <div className='pt-2'>
                <FormControl
                  className=''
                  isInvalid={touched.dob && errors.dob ? true : false}
                  type='date'
                  value={values.dob}
                  isValid={touched.dob && !errors.dob}
                  name='dob'
                  onChange={handleChange}
                  size='sm'
                  placeholder={`Date of Birth`}
                />
              </div>
              <div className='pt-2'>
                <FormControl
                  isInvalid={touched.dateOfJoining && errors.dateOfJoining ? true : false}
                  isValid={touched.dateOfJoining && !errors.dateOfJoining}
                  className=''
                  type='date'
                  value={values.dateOfJoining}
                  onChange={handleChange}
                  name='dateOfJoining'
                  size='sm'
                  placeholder={`Date of joining.`}
                />
              </div>
              <div className='pt-2'>
                <FormControl
                  className=''
                  type='text'
                  value={values.emergency_mobile_no}
                  onChange={handleChange}
                  name='emergency_mobile_no'
                  size='sm'
                  placeholder={`Emergency Mobile no`}
                />
              </div>

              <div className='pt-2'>
                <FormControl
                  value={values.city}
                  className=''
                  type='text'
                  onChange={handleChange}
                  name='city'
                  size='sm'
                  placeholder={`City`}
                />
              </div>

              <div className='pt-2'>
                <FormControl
                  value={values.state}
                  className=''
                  type='text'
                  onChange={handleChange}
                  name='state'
                  size='sm'
                  placeholder={`state`}
                />
              </div>

              <div className='pt-2'>
                <FormControl
                  value={values.country}
                  className=''
                  type='text'
                  onChange={handleChange}
                  name='country'
                  size='sm'
                  placeholder={`country`}
                />
              </div>
              <div className='pt-2'>
                <FormControl
                  isInvalid={touched.excercise && errors.excercise ? true : false}
                  isValid={touched.excercise && !errors.excercise}
                  name='excercise'
                  as='select'
                  value={values.excercise}
                  onChange={handleChange}
                  size='sm'
                >
                  <option value=''> select </option>
                  <option value='cardio'> Cardio </option>
                  <option value='body building'> Body Building </option>
                  <option value='weight gain'> Weight Gain </option>
                  <option value='strength Gaining'> Strength Gaining </option>
                  <option value='belly fat'> Belly fat </option>
                </FormControl>
              </div>

              <div className='pt-2'>
                <FormControl
                  value={values.profile_pic}
                  size='sm'
                  onChange={handleChange}
                  type='file'
                  name='profile_pic'
                  placeholder='image'
                />
              </div>
            </div>
          </Modal.Body>
          <Modal.Footer>
            <Button size='sm' variant='danger' onClick={props.onHide}>
              Cancel
            </Button>
            <Button size='sm' variant='warning' onClick={handleReset}>
              Reset
            </Button>

            <Button disabled={loading} type='submit' size='sm' variant='success'>
              Save
              {loading && <Spinner className='pl-2' animation='border' size='sm' />}
            </Button>
          </Modal.Footer>
        </Form>
      </Modal>
    </div>
  )
}

export default AddMemberModal
