import axios, { AxiosInstance } from 'axios'

let request: AxiosInstance

console.log('process.env', process.env)

if (process.env.NODE_ENV === 'development') {
  request = axios.create({
    baseURL: 'http://localhost:5000/',
  })
} else {
  request = axios.create({
    baseURL: '/',
  })
}

request.prototype.setToken = () => {
  request.defaults.headers.common.Authorization = `Bearer ${localStorage.getItem('token')}`
}

request.interceptors.response.use(res => {
  if (res.status === 401) {
    console.log('Unauthorized..')
    window.location.pathname = '/'
  }
  return res
})

export default request
