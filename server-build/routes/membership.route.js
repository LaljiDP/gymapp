"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _express = _interopRequireDefault(require("express"));

var _passport = _interopRequireDefault(require("passport"));

var _membership = _interopRequireDefault(require("../models/membership.schema"));

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { (0, _defineProperty2.default)(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

var router = _express.default.Router();

router.post('/', _passport.default.authenticate('jwt', {
  session: false
}),
/*#__PURE__*/
function () {
  var _ref = (0, _asyncToGenerator2.default)(
  /*#__PURE__*/
  _regenerator.default.mark(function _callee(req, res) {
    var membership;
    return _regenerator.default.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            if (!req.user) res.status(401).json({
              message: 'Unauthorized'
            });
            _context.prev = 1;
            _context.next = 4;
            return new _membership.default(_objectSpread({}, req.body)).save();

          case 4:
            membership = _context.sent;

            if (membership) {
              res.send(membership);
            } else {
              res.status(403).json({
                message: 'Bad request !'
              });
            }

            _context.next = 11;
            break;

          case 8:
            _context.prev = 8;
            _context.t0 = _context["catch"](1);
            res.status(501).json({
              message: 'Error while fetching Membership data',
              trace: _context.t0.toString()
            });

          case 11:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, null, [[1, 8]]);
  }));

  return function (_x, _x2) {
    return _ref.apply(this, arguments);
  };
}());
router.get('/:userId/:page/:limit', _passport.default.authenticate('jwt', {
  session: false
}),
/*#__PURE__*/
function () {
  var _ref2 = (0, _asyncToGenerator2.default)(
  /*#__PURE__*/
  _regenerator.default.mark(function _callee2(req, res) {
    var _req$params, userId, page, limit, membership;

    return _regenerator.default.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            if (!req.user) res.status(401).json({
              message: 'Unauthorized'
            });
            _req$params = req.params, userId = _req$params.userId, page = _req$params.page, limit = _req$params.limit;
            console.log('req.params', req.params);
            _context2.prev = 3;

            if (!userId) {
              _context2.next = 11;
              break;
            }

            _context2.next = 7;
            return _membership.default.find({
              user: userId
            }, null, {
              skip: Number(page) * Number(limit),
              limit: Number(limit)
            }).populate('user');

          case 7:
            membership = _context2.sent;

            if (membership) {
              res.send(membership);
            } else {
              res.send([]);
            }

            _context2.next = 12;
            break;

          case 11:
            res.status(403).json({
              message: 'Bad Request!'
            });

          case 12:
            _context2.next = 17;
            break;

          case 14:
            _context2.prev = 14;
            _context2.t0 = _context2["catch"](3);
            res.status(501).json({
              message: 'Error while fetching memberships data',
              trace: _context2.t0.toString()
            });

          case 17:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2, null, [[3, 14]]);
  }));

  return function (_x3, _x4) {
    return _ref2.apply(this, arguments);
  };
}());
var _default = router;
exports.default = _default;
//# sourceMappingURL=membership.route.js.map