import * as React from 'react'
import './Header.scss'
import menu from '../../assets/menu.svg'
import avtar from '../../assets/avtar.svg'
import { Link } from 'react-router-dom'
import { Dropdown } from 'react-bootstrap'

interface HeaderPropsI {
  toggleExpand: any
}

const Header: React.FC<HeaderPropsI> = (props): React.ReactElement => {
  const [visible, setVisible] = React.useState(false)

  const toggleMenuVisibility = () => setVisible(!visible)

  return (
    <div className='header'>
      <div className='header-container'>
        <div className='left-side pl-2 pointer'>
          <i className='menu-icon' onClick={props.toggleExpand}>
            <img src={menu} alt='menu' className='text-primary' />
          </i>
        </div>
        <div className='right-side pr-2 position-relative'>
          <Dropdown>
            <Dropdown.Toggle className='p-0 custom-dd-design' variant='secondary' id='dropdown-split-basic'>
              <div onClick={toggleMenuVisibility} className='avatar'>
                <img src={avtar} alt='avtar' />
              </div>
            </Dropdown.Toggle>
            <Dropdown.Menu>
              <Dropdown.Item>
                <Link to='/admin/settings'>
                  <i className='fa fa-cog mr-2' />
                  Settings
                </Link>
              </Dropdown.Item>
              <Dropdown.Item>
                <Link to='/logout'>
                  <i className='fa fa-sign-out mr-2' />
                  Logout
                </Link>
              </Dropdown.Item>
            </Dropdown.Menu>
          </Dropdown>
        </div>
      </div>
    </div>
  )
}

export default Header
