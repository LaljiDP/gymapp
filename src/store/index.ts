import { configureStore, ThunkAction, Action } from '@reduxjs/toolkit'
import { RootState, rootReducer } from './reducers'
import { StoreI } from './store.types'
import { initialMemberData } from './reducers/memberDetails.reducer'

const initialStore: StoreI = {
  members: {
    data: [],
    loading: false,
    page: 0,
    total: 0,
  },
  users: {
    data: [],
    trainers: [],
    loading: false,
    page: 0,
    total: 0,
  },
  dashboard: {
    loading: false,
    members: 0,
    users: 0,
  },
  memberDetails: {
    data: initialMemberData,
    loading: false,
    payments: [],
    memberships: [],
  },
  schedule: {
    data: [],
    loading: false,
    page: 0,
    total: 0,
  },
}

export const store = configureStore({
  reducer: rootReducer,
  preloadedState: initialStore,
})

if (process.env.NODE_ENV === 'development' && module.hot) {
  module.hot.accept('./reducers', () => {
    const newRootReducer = require('./reducers').default
    store.replaceReducer(newRootReducer)
  })
}

export type AppThunk = ThunkAction<void, RootState, null, Action<string>>

export const dispatch = store.dispatch

export type AppDispatch = typeof store.dispatch
