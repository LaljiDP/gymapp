import * as React from 'react'
import { useFormik, FormikValues } from 'formik'
import _get from 'lodash/get'
import * as yup from 'yup'
import { Modal, Form, FormControl, Spinner } from 'react-bootstrap'
import { Button } from 'react-bootstrap'
import { createSchedule } from '../../store/actions/schedule.action'
import { toast } from 'react-toastify'
import { updateMemberData } from '../../store/actions/member.action'
import { ScheduleI } from '../../store/store.types'

interface ScheduleModalPropsI {
  open: boolean
  onHide: () => void
  update?: boolean
  isUpdate?: boolean
  memberId?: string
  scheduleData?: ScheduleI[]
}

const initialValues = {
  monday: '',
  tuesday: '',
  wednesday: '',
  thursday: '',
  friday: '',
  saturday: '',
}

const scheduleSchema = yup.object().shape({
  monday: yup.string().required(),
  tuesday: yup.string().required(),
  wednesday: yup.string().required(),
  thursday: yup.string().required(),
  friday: yup.string().required(),
  saturday: yup.string().required(),
})

const ScheduleModal: React.FC<ScheduleModalPropsI> = (props): React.ReactElement => {
  const [loading, setLoading] = React.useState(false)
  const [initial, setInitial] = React.useState(initialValues)

  React.useEffect(() => {
    const dd: any = props.scheduleData?.map(s => ({ [s.day]: s.excercise }))
    setInitial(dd)
  }, [props.scheduleData])

  console.log('initial', initial)

  const onSubmit = (values: FormikValues) => {
    setLoading(true)
    const schedule = Object.keys(values).map(function(day) {
      return { day, excercise: values[day] }
    })
    if (props.update) {
      updateMemberData(props.memberId || '', { schedule }, (err: boolean, msg: string) => {
        if (err) {
          toast.error(msg, { position: toast.POSITION.TOP_CENTER })
        } else {
          toast.success(msg, { position: toast.POSITION.TOP_CENTER })
        }
        setLoading(false)
        props.onHide()
      })
    } else {
      createSchedule({ schedule }, (error, msg) => {
        if (error) {
          createSchedule({ schedule }, (error, msg) => {
            if (error) {
              toast.error(msg, { position: toast.POSITION.TOP_CENTER })
            } else {
              toast.success(msg, { position: toast.POSITION.TOP_CENTER })
              props.onHide()
              setLoading(false)
            }
          })
        } else {
          toast.success(msg, {
            position: toast.POSITION.TOP_CENTER,
          })
          props.onHide()
        }
        setLoading(false)
      })
    }
  }

  const { handleSubmit, handleChange, values, errors, touched } = useFormik({
    initialValues,
    validationSchema: scheduleSchema,
    onSubmit,
    enableReinitialize: true,
  })

  return (
    <Modal centered={true} show={props.open} onHide={props.onHide}>
      <Modal.Header>{props.isUpdate ? 'Update Schedule' : 'Add new schedule'}</Modal.Header>
      <Form onSubmit={handleSubmit}>
        <Modal.Body>
          <div className='p-2'>
            {Object.keys(initialValues).map(day => (
              <Form.Group key={day}>
                <Form.Label>
                  <span className='text-capitalize'>{day}</span>
                </Form.Label>
                <FormControl
                  name={day}
                  onChange={handleChange}
                  value={_get(values, day)}
                  isValid={_get(touched, day) && !_get(errors, day)}
                  isInvalid={_get(touched, day) && _get(errors, day) ? true : false}
                  as='select'
                >
                  <option value=''> Select</option>
                  <option value='Mixed'>Mixed</option>
                  <option value='Chest'>Chest</option>
                  <option value='Back'>Back</option>
                  <option value='Shoulder'>Shoulder</option>
                  <option value='Biceps'>Biceps</option>
                  <option value='Leg'>Leg</option>
                  <option value='Cardio'>Cardio</option>
                </FormControl>
              </Form.Group>
            ))}
          </div>
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={props.onHide} variant='secondary'>
            Cancel
          </Button>
          <Button disabled={loading} type='submit' variant='primary'>
            Save
            {loading && <Spinner className='ml-2' size='sm' animation='border' />}
          </Button>
        </Modal.Footer>
      </Form>
    </Modal>
  )
}

export default ScheduleModal
