import React from 'react'
import { Route, BrowserRouter as Router, Switch } from 'react-router-dom'
import DashboardContainer from '../Dashboard'
import Header from '../Header'
import './common.scss'
import SideBar from '../SideBar'
import Members from '../Members'
import Users from '../Users'
import MemberDetails from '../MemberDetails'
import Logout from '../Common/Logout'
import Settings from '../settings'

const AdminRoutes: React.FC = (): React.ReactElement => {
  const [expand, setExpand] = React.useState(false)

  const toggleExpand = () => {
    setExpand(!expand)
  }
  return (
    <Router>
      <div className='root-container'>
        {<SideBar expand={expand} toggleExpand={toggleExpand} />}

        <Header toggleExpand={setExpand} />
        <div className='card card-shadow position-relative container-body d'>
          <Switch>
            <Route path='/admin/dashboard' exact={true} component={DashboardContainer} />
            <Route path='/admin/members' exact={true} component={Members} />
            <Route path='/admin/members/:id' exact={true} component={MemberDetails} />
            <Route path='/admin/users' exact={true} component={Users} />
            <Route path='/admin/membership_calc' exact={true} component={() => <h5> Under development </h5>} />
            <Route path='/admin/settings' exact={true} component={Settings} />
            <Route path='/logout' exact={true} component={Logout} />
            <Route component={() => <h4> Not found. </h4>} />
          </Switch>
        </div>
      </div>
    </Router>
  )
}

export default AdminRoutes
