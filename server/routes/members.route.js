import express from 'express'
import passport from 'passport'
import Member from '../models/members.schema'

const router = express.Router()

router.post('/', passport.authenticate('jwt', { session: false }), async (req, res) => {
  if (!req.user) res.status(401).json({ message: 'Unauthorized' })
  try {
    const member = await new Member({
      ...req.body,
    }).save()

    if (member) {
      res.send(member)
    } else {
      res.status(501).json({ message: 'Something wrong..' })
    }
  } catch (err) {
    res.status(501).json({ message: err.toString() })
  }
})

router.get('/:id/single', passport.authenticate('jwt', { session: false }), async (req, res) => {
  if (!req.user) res.status(401).json({ message: 'Unauthorized' })
  const { id } = req.params
  try {
    const member = await Member.findById(id).populate('trainer')
    res.send(member)
  } catch (err) {
    res.status(501).json({ message: 'Error while fetching member information..', trace: err.toString() })
  }
})

router.get('/total', passport.authenticate('jwt', { session: false }), async (req, res) => {
  if (!req.user) res.status(401).json({ message: 'Unauthorized' })
  try {
    const total = await Member.count()
    res.send({ total })
  } catch (err) {
    res.status(501).json({ message: err.toString() })
  }
})

router.put('/:id', passport.authenticate('jwt', { session: false }), async (req, res) => {
  if (!req.user) res.status(401).json({ message: 'Unauthorized' })
  const { id } = req.params

  const conditions = { _id: id },
    update = { ...req.body },
    options = { multi: true }

  try {
    const member = await Member.update(conditions, update, options)
    if (member.ok === 1) {
      const result = await Member.findById(id).populate('trainer')
      res.send(result)
    } else {
      console.log('member', member)
    }
  } catch (err) {
    console.log(err)
    res.status(501).json({ message: 'Error while updating Member Docs.', trace: err.toString() })
  }
})

router.delete('/:id', passport.authenticate('jwt', { session: false }), async (req, res) => {
  if (!req.user) res.status(401).json({ message: 'Unauthorized' })
  const { id } = req.params
  try {
    const member = await Member.deleteOne({ _id: id })
    res.send(member)
  } catch (err) {
    res.status(501).json({ message: 'Somthing wrong', trace: err.toString() })
  }
})

router.get('/:page', passport.authenticate('jwt', { session: false }), async (req, res) => {
  if (!req.user) res.status(401).json({ message: 'Unauthorized' })

  const { page } = req.params

  console.log('page', page)

  if (!page) res.status(422).json({ message: 'Bad Request ! parameter required' })
  try {
    const members = await Member.find({}, null, { skip: page * 10, limit: 10 }).sort({ createdAt: 'desc' })
    const totalRecord = await Member.count()
    res.send({ members, total: totalRecord })
  } catch (err) {
    res.status(501).json({ message: 'Error while fetching members.', stacktrace: err.toString() })
  }
})

export default router
