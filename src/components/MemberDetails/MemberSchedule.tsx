import * as React from 'react'
import { Transition } from 'react-transition-group'
import { ScheduleI } from '../../store/store.types'

interface MemberSchedulePropsI {
  openScheduleModal: () => void
  data?: ScheduleI[] | null
  ispUdate?: boolean
  handleCollapse: (data: any) => void
  expand: boolean
}

const MemberSchedule: React.FC<MemberSchedulePropsI> = React.memo(
  ({ openScheduleModal, data, ispUdate, expand, handleCollapse }): React.ReactElement => {
    return (
      <div className='member-schedule'>
        <div className='alert font-weight-bold alert-primary'>
          <div className='d-flex justify-content-between'>
            <div>Schedule</div>
            <div className='schedule-icons'>
              {ispUdate ? (
                <i onClick={openScheduleModal} className='fa fa-edit' />
              ) : (
                <i onClick={openScheduleModal} className='fa fa-plus' />
              )}
              <i
                onClick={() => handleCollapse({ schedule: !expand })}
                className={`fa ${expand ? 'fa-angle-down' : 'fa-angle-right'} pl-4`}
              />
            </div>
          </div>
        </div>
        <Transition in={expand} timeout={1000}>
          {state => (
            <div className={`schedule-container ${expand ? 'd-block' : 'd-none'}`}>
              <div className='p-3'>
                {data?.map(s => (
                  <div className='pl-3 pr-3 day-row' key={s._id}>
                    <div className='day text-capitalize'>{s?.day}</div>
                    <div className='excercise'>{s?.excercise}</div>
                  </div>
                ))}
              </div>
            </div>
          )}
        </Transition>
      </div>
    )
  },
  (pp, np) => pp.data === np.data && pp.expand === np.expand
)

export default MemberSchedule
