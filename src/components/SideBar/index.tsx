import * as React from 'react'
import { withRouter, RouteComponentProps } from 'react-router'
import './sidebar.scss'
import defaultLogo from '../../assets/logo.svg'

interface SideBarPropsI extends RouteComponentProps {
  toggleExpand: any
  expand: boolean
}

const SideBar: React.FC<SideBarPropsI> = (props): React.ReactElement => {
  const sideBarRef: any = React.useRef()
  const sideBarDiv: any = React.useRef()

  const handleClickOutside = (event: MouseEvent) => {
    if (sideBarRef.current && !sideBarRef.current.contains(event.target)) {
      props.toggleExpand()
    }
  }

  React.useEffect(() => {
    // Bind the event listener
    props.expand && document.addEventListener('mousedown', handleClickOutside)
    return () => {
      // Unbind the event listener on clean up
      document.removeEventListener('mousedown', handleClickOutside)
    }
  })

  React.useEffect(() => {
    const { current } = sideBarRef
    if (props.expand) {
      current.classList.add('sideWidth')
      sideBarDiv.current.style.display = 'block'
      return
    } else {
      current.classList.remove('sideWidth')
      sideBarDiv.current.style.display = 'none'
    }
  })

  const goto = (path: string) => {
    props.toggleExpand()
    props.history.push(path)
  }

  return (
    <div ref={sideBarRef} className={`sidebar-container vh-100`}>
      <div ref={sideBarDiv} className='sidebar-div'>
        <div className='sidebar-header card-shadow'>
          <div className='left-side p-2'>Gym App</div>
          <div className='right-side p-2'>
            <h6>
              <span onClick={props.toggleExpand} className='icon font-weight-bold fs-18 pl-2 pt-2 h2 text-white'>
                &#215;
              </span>
            </h6>
          </div>
        </div>
        <div className='sidebar-body p-2'>
          <div className='app-logo mb-4'>
            <img src={defaultLogo} height='70' width='120' className='img' alt='img' />
          </div>
          <div className='sidebar-menu pt-2 pb-2'>
            <ul>
              <li onClick={() => goto('/admin/dashboard')}>
                <i className='fa fa-dashboard pl-2' />
                <span className='pl-3'>Dashboard</span>
              </li>
              <li onClick={() => goto('/admin/members')}>
                <i className='fa fa-users pl-2' />
                <span className='pl-3'>Members</span>
              </li>
              <li onClick={() => goto('/admin/users')}>
                <i className='fa fa-tumblr-square pl-2' />
                <span className='pl-3'>Users / Trainers</span>
              </li>

              <li onClick={() => goto('/admin/membership_calc')}>
                <i className='fa fa-rupee pl-2' />
                <span className='pl-3'>Need Membership Renew</span>
              </li>
              <li onClick={() => goto('/admin/settings')}>
                <i className='fa fa-cog pl-2' />
                <span className='pl-3'>Settings</span>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  )
}

export default withRouter(SideBar)
