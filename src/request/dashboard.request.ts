import request from './index'

request.defaults.headers.common.Authorization = `Bearer ${localStorage.getItem('token')}`

export const fetchUserInfo = () => {
  request.defaults.headers.common.Authorization = `Bearer ${localStorage.getItem('token')}`
  return request.get('/api/user/info')
}
