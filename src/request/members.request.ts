import request from './index'

export const fetchMembersAPI = (page: number) => {
  return request(`/api/members/${page}`)
}
