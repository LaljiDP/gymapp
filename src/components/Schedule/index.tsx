import * as React from 'react'
import TogglePlusButton from '../Common/TogglePlusButton'
import ScheduleModal from '../Modals/ScheduleModal'
import { fetchSchedule } from '../../store/actions/schedule.action'
import { useSelector } from 'react-redux'
import { StoreI } from '../../store/store.types'
import Loading from '../Common/Loading'
import ScheduleLists from './ScheduleLists'
import './schedule.scss'

const Schedule: React.FC = (): React.ReactElement => {
  const [scheduleModal, setScheduleModal] = React.useState(false)

  const { loading, data } = useSelector((state: StoreI) => state.schedule)

  React.useEffect(() => {
    fetchSchedule(0)
  }, [])

  const toggleScheduleModal = () => setScheduleModal(!scheduleModal)

  if (loading) {
    return <Loading centered={true} />
  }

  return (
    <div className='schedule-container'>
      <TogglePlusButton actionFC={toggleScheduleModal} />
      <ScheduleLists data={data} />
      <ScheduleModal open={scheduleModal} onHide={toggleScheduleModal} />
    </div>
  )
}

export default Schedule
