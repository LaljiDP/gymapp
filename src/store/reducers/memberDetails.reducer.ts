import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import { ActionI, memberDetailsReducerI, MemberShipI } from '../store.types'

export const initialMemberData = {
  _id: '',
  name: '',
  dob: '',
  address: '',
  weight: 0,
  height: '',
  email: '',
  blood_group: '',
  city: '',
  state: '',
  profile_pic: '',
  country: '',
  mobile_no: '',
  emergency_mobile_no: '',
  schedule: [],
  excercise: '',
  dateOfJoining: '',
  isDeleted: false,
  createdAt: '',
  updatedAt: '',
}

const initialState: memberDetailsReducerI = {
  data: initialMemberData,
  loading: false,
  payments: [],
  memberships: [],
}

export const memberDetailsSlice = createSlice({
  name: 'memberDetails',
  initialState,
  reducers: {
    storeMemberInfo: (state, action: ActionI) => {
      state.data = action.payload
    },
    updateMemberDetails: (state, action: ActionI) => {
      state.data = action.payload
    },
    memberDetailloading: (state, action: ActionI) => {
      console.log('calling...')
      state.loading = action.payload
    },
    storeMembership: (state, action: PayloadAction<MemberShipI[]>) => {
      state.memberships = action.payload
    },
    addMembership: (state, action: PayloadAction<MemberShipI>) => {
      state.memberships.unshift(action.payload)
    },
  },
})

export const {
  addMembership,
  storeMemberInfo,
  updateMemberDetails,
  memberDetailloading,
  storeMembership,
} = memberDetailsSlice.actions

export const memberDetailsReducer = memberDetailsSlice.reducer
