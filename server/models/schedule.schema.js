import mongoose from 'mongoose'
const Schema = mongoose.Schema

const scheduleSchema = new Schema({
  schedule: [{ day: String, excercise: String }],
  createdAt: { type: Date, default: Date.now },
  updatedAt: { type: Date, default: Date.now },
})

export default mongoose.model('Schedule', scheduleSchema)
