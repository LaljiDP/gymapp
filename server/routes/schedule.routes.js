import express from 'express'
import Schedule from '../models/schedule.schema'
import passport from 'passport'

const router = express.Router()

router.get('/:page', passport.authenticate('jwt', { session: false }), async (req, res) => {
  if (!req.user) res.status(401).json({ message: 'Unatuhorized' })
  const { page } = req.params
  try {
    const schedule = await Schedule.find({}, null, { skip: page * 10, limit: 10 })
    if (schedule) {
      res.send(schedule)
    }
  } catch (err) {
    res.status(501).json({ message: 'Error while fetching Schedule', trace: err.toString() })
  }
})

router.post('/', passport.authenticate('jwt', { session: false }), async (req, res) => {
  if (!req.user) res.status(401).json({ message: 'Unatuhorized' })
  const { schedule } = req.body
  try {
    const schedle = await new Schedule({
      schedule: schedule,
    }).save()
    if (schedle) {
      res.send(schedle)
    }
  } catch (err) {
    res.status(501).json({ message: 'Error while saving schedule.', trace: err.toString() })
  }
})

export default router
