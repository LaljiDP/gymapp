export interface UserI {
  role?: string
  _id: string
  name: string
  members?: any
  userName: string
  email: string
  profile_pic?: string
  dob: string
  password?: string
  lastLoggedIn: string
  createdAt: string
  updatedAt: string
}
