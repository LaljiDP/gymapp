import * as React from 'react'
import { Redirect } from 'react-router'
import Loading from './Loading'

const Logout = () => {
  const [loading, setLoading] = React.useState(true)

  setTimeout(() => {
    setLoading(false)
  }, 1500)

  React.useEffect(() => {
    localStorage.removeItem('token')
    window.location.pathname = '/'
    return () => {
      console.log('Logout....')
    }
  }, [])

  if (loading) {
    return <Loading centered={true} />
  }
  localStorage.removeItem('token')

  return (
    <div>
      <Redirect to='/' />
    </div>
  )
}

export default Logout
