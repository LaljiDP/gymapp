import { createSlice } from '@reduxjs/toolkit'
import { UserReducerI } from '../store.types'

interface ActionI {
  type: string
  payload: any
}

const initialState: UserReducerI = {
  data: [],
  trainers: [],
  loading: false,
  page: 0,
  total: 0,
}

export const userSlice = createSlice({
  name: 'users',
  initialState: initialState,
  reducers: {
    storeUsers: (state, action: ActionI) => {
      state.data = action.payload.users
      state.total = action.payload.total
      state.page = action.payload.page
    },
    addUsersToStore: (state, action: ActionI) => {
      let users = state.data.slice()
      if (users.length === 10 && state.page === 0) {
        users.unshift(action.payload)
        users.pop()
        state.data = [...users]
        state.total = state.total + 1
      } else {
        users.unshift(action.payload)
        state.total = state.total + 1
      }
    },
    storeTrainers: (state, action: ActionI) => {
      state.trainers = action.payload
    },
    removeUser: (state, action: ActionI) => {
      let users = state.data.slice()
      users.splice(
        users.findIndex(o => o._id === action.payload),
        1
      )
      state.data = users
    },
    loading: (state, action: any) => {
      state.loading = action.payload
    },
  },
})

export const { storeUsers, storeTrainers, removeUser, loading, addUsersToStore } = userSlice.actions

export const userReducer = userSlice.reducer
