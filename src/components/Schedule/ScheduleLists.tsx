import * as React from 'react'
import { ScheduleData } from '../../store/store.types'

interface ScheduleListsPropsI {
  data: ScheduleData[]
}

const ScheduleLists: React.FC<ScheduleListsPropsI> = ({ data }): React.ReactElement => {
  return (
    <div className='schedule-lists'>
      {data?.map(s => (
        <div className='schedule-card p-2'>
          <div className='table-responsive-sm'>
            <table className='table-sm striped ml-auto mr-auto'>
              <tr>
                {s.schedule.map(ss => (
                  <th>{ss.day.substr(0, 3)}</th>
                ))}
              </tr>

              <tr>
                {s.schedule.map(ss => (
                  <td>{ss.excercise}</td>
                ))}
              </tr>
            </table>
          </div>
        </div>
      ))}
    </div>
  )
}

export default ScheduleLists
