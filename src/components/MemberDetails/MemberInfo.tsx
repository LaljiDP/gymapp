import * as React from 'react'
import moment from 'moment'
import { MemberI } from '../../store/store.types'

interface MemberInfoPropsI {
  data: MemberI
}

const MemberInfo: React.FC<MemberInfoPropsI> = ({ data }): React.ReactElement => {
  return (
    <>
      <div className='member-info'>
        <div className='key'>Name</div>
        <div className='value'> {data.name} </div>
      </div>
      <div className='member-info'>
        <div className='key'>Address</div>
        <div className='value'>{data.address}</div>
      </div>
      <div className='member-info'>
        <div className='key'>Mobile no</div>
        <div className='value'>{data.mobile_no || '---'}</div>
      </div>
      <div className='member-info'>
        <div className='key'>Blood group</div>
        <div className='value'>{data.blood_group || '---'}</div>
      </div>
      <div className='member-info'>
        <div className='key'>Height</div>
        <div className='value'>{data.height || '---'}</div>
      </div>
      <div className='member-info'>
        <div className='key'>Weight</div>
        <div className='value'>{data.weight || '---'}</div>
      </div>
      <div className='member-info'>
        <div className='key'>Excercise</div>
        <div className='value'>{data.excercise || '---'}</div>
      </div>
      <div className='member-info'>
        <div className='key'>Date of Joining</div>
        <div className='value'>{moment(data.dateOfJoining).format('ll') || '---'}</div>
      </div>
      <div className='member-info'>
        <div className='key'>Emergency contact no</div>
        <div className='value'>{data.emergency_mobile_no || '---'}</div>
      </div>
      <div className='member-info'>
        <div className='key'>Birth Date</div>
        <div className='value'>{moment(data.dob).format('ll') || '---'}</div>
      </div>
      <div className='member-info'>
        <div className='key'>Member Added To</div>
        <div className='value'>{moment(data.createdAt).format('YYYY-MM-DD') || '---'}</div>
      </div>
    </>
  )
}

export default MemberInfo
