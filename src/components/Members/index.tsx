import * as React from 'react'
import './members.scss'
import { FormControl } from 'react-bootstrap'
import AddMemberModal from '../Modals/AddMemberModal'
import MembersList from './MemberList'
import TogglePlusButton from '../Common/TogglePlusButton'
import { fetchMembers } from '../../store/actions/member.action'
import { useSelector } from 'react-redux'
import { StoreI, MemberI } from '../../store/store.types'
import Loading from '../Common/Loading'
import Pagination from '../Common/Pagination'

interface MembersPropsI {
  members: MemberI[]
}

const Members: React.FC<MembersPropsI> = (props): React.ReactElement => {
  const [addMemberModal, setAddMemberModal] = React.useState(false)

  const members = useSelector((state: StoreI) => state.members)

  const totalPage = Math.ceil(members.total / 10)

  const [hasNext, setHasNext] = React.useState(totalPage > members.page + 1)

  const [hasPrev, setHasPrev] = React.useState(members.page >= 1)

  React.useEffect(() => {
    setHasNext(totalPage > members.page + 1)
  }, [members.page, members.total, totalPage])

  React.useEffect(() => {
    setHasPrev(members.page >= 1)
  }, [hasNext, members.page])

  React.useEffect(() => {
    fetchMembers(0)
  }, [])

  const handleNextPageChange = () => {
    if (hasNext) {
      fetchMembers(members.page + 1)
    }
  }

  const handlePrevPageChange = () => {
    if (hasPrev) {
      fetchMembers(members.page - 1)
    }
  }

  const toggleMemberModal = () => {
    setAddMemberModal(!addMemberModal)
  }

  return (
    <div className='members-container'>
      <TogglePlusButton actionFC={toggleMemberModal} />
      <div className='member-body'>
        <div className='member-search m-3'>
          <FormControl type='search' name='search' size='sm' placeholder='search members..' />
          <Pagination
            handlePrevPageChange={handlePrevPageChange}
            handleNextPageChange={handleNextPageChange}
            hasNext={hasNext}
            hasPrev={hasPrev}
            currentPage={members.page + 1}
            totalPage={totalPage}
          />
        </div>
        {members.loading ? (
          <Loading className='mt-5' />
        ) : (
          <MembersList data={members.data} loading={members.loading} page={members.page} />
        )}
      </div>
      <AddMemberModal show={addMemberModal} onHide={toggleMemberModal} />
    </div>
  )
}

export default Members
