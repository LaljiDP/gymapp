"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _express = _interopRequireDefault(require("express"));

var _bcryptjs = _interopRequireDefault(require("bcryptjs"));

var _jsonwebtoken = _interopRequireDefault(require("jsonwebtoken"));

var _users = _interopRequireDefault(require("../models/users.schema"));

var router = _express.default.Router();
/* GET users listing. */


router.post('/login',
/*#__PURE__*/
function () {
  var _ref = (0, _asyncToGenerator2.default)(
  /*#__PURE__*/
  _regenerator.default.mark(function _callee(req, res) {
    var _req$body, username, password, user, isPasswordCorrect, token;

    return _regenerator.default.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _req$body = req.body, username = _req$body.username, password = _req$body.password;
            _context.next = 3;
            return _users.default.findOne({
              userName: username
            });

          case 3:
            user = _context.sent;
            console.log('user', user);

            if (user) {
              isPasswordCorrect = _bcryptjs.default.compareSync(password, user.password);

              if (isPasswordCorrect) {
                token = _jsonwebtoken.default.sign({
                  user: user
                }, process.env.PRIVATE_KEY);
                res.send({
                  token: token
                });
              } else {
                res.status(401).json({
                  message: 'Invalid credentials'
                });
              }
            } else {
              res.status(401).json({
                message: 'Unauthorized'
              });
            }

          case 6:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));

  return function (_x, _x2) {
    return _ref.apply(this, arguments);
  };
}());
var _default = router;
exports.default = _default;
//# sourceMappingURL=auth.routes.js.map