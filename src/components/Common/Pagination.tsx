import * as React from 'react'

interface PaginationI {
    handlePrevPageChange: () => void,
    handleNextPageChange: () => void,
    hasPrev: boolean,
    hasNext: boolean,
    totalPage: number,
    currentPage: number
}

const Pagination: React.FC<PaginationI> = (props): React.ReactElement => {
    const { handlePrevPageChange, hasPrev, hasNext, handleNextPageChange, totalPage, currentPage  } = props
    return (
        <div className='pagination mt-2 d-flex justify-content-between p-2'>
            <div>
                {' '}
                <i onClick={handlePrevPageChange} className={`fa fa-chevron-left ${hasPrev ? 'text-primary' : 'text-grey'}`} aria-hidden='true' />{' '}
            </div>
            <div>
                {' '}
                Page: {currentPage} of {totalPage}
            </div>
            <div>
                <i onClick={handleNextPageChange} className={`fa fa-chevron-right ${hasNext ? 'text-primary' : 'text-grey'}`} aria-hidden='true' />
            </div>
        </div>
    )
}

export default Pagination