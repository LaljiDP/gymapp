import request from './index'

export const login = (payload: any) => {
  return request.post('/api/auth/login', payload)
}
