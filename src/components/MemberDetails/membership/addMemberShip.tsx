import * as React from 'react'
import moment from 'moment'
import * as yup from 'yup'
import { Modal, Button, Form, FormControl, FormGroup, Col, Spinner } from 'react-bootstrap'
import { useFormik, FormikValues } from 'formik'
import { toast } from 'react-toastify'
import { createMembershipForMember } from '../../../store/actions/member.action'

interface AddMembershipModalPropI {
  show: boolean
  onHide: () => void
  userId: string
}

const initialValues = {
  plan: '',
  startDate: '',
  amount: '',
}

const schema = yup.object().shape({
  plan: yup.string().required(),
  startDate: yup.string().required(),
  amount: yup.number().required(),
})

const AddMembershipModal: React.FC<AddMembershipModalPropI> = (props): React.ReactElement => {
  const [loading, setLoading] = React.useState(false)

  const onSubmit = (payload: FormikValues) => {
    setLoading(true)
    console.log('payload', payload)
    const requestPayload = { ...payload }
    requestPayload.endDate = moment(payload.startDate)
      .add(payload.plan, 'month')
      .format('YYYY-MM-DD')
    requestPayload.months = payload.plan
    requestPayload.plan = `${payload.plan} months`
    requestPayload.user = props.userId
    console.log('requestPayload', requestPayload)
    createMembershipForMember(requestPayload, (error, msg) => {
      if (error) {
        toast.error(msg, { position: toast.POSITION.TOP_CENTER })
      } else {
        toast.success(msg, { position: toast.POSITION.TOP_CENTER })
        props.onHide()
      }
      setLoading(false)
    })
  }

  const { handleSubmit, handleChange, touched, errors, values } = useFormik({
    initialValues,
    validationSchema: schema,
    onSubmit,
  })
  return (
    <Modal centered={true} show={props.show} onHide={props.onHide}>
      <Modal.Header> Add Membership </Modal.Header>
      <Form onSubmit={handleSubmit}>
        <Modal.Body>
          <div className='add-membership-container'>
            <Form.Row>
              <Col>
                <label> Start Date </label>
                <FormControl
                  isValid={touched.startDate && !errors.startDate}
                  isInvalid={touched.startDate && errors.startDate ? true : false}
                  name='startDate'
                  type='Date'
                  value={values.startDate}
                  onChange={handleChange}
                />
              </Col>
              <Col>
                <label> Select plan </label>
                <FormControl
                  isValid={touched.plan && !errors.plan}
                  isInvalid={touched.plan && errors.plan ? true : false}
                  as='select'
                  name='plan'
                  values={values.plan}
                  onChange={handleChange}
                >
                  <option>select plan</option>
                  <option value='1'>1 Month</option>
                  <option value='3'>3 Months</option>
                  <option value='6'>6 Months</option>
                  <option value='12'>1 Year</option>
                </FormControl>
              </Col>
            </Form.Row>
            <FormControl
              type='number'
              className='mt-2'
              name='amount'
              placeholder='Amount'
              value={values.amount}
              onChange={handleChange}
              isValid={touched.amount && !errors.amount}
              isInvalid={touched.amount && errors.amount ? true : false}
            />
          </div>
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={props.onHide} size='sm' variant='secondary'>
            cancel
          </Button>
          <Button type='submit' size='sm' variant='primary'>
            Save
            {loading && <Spinner size='sm' animation='border' className='ml-2' />}
          </Button>
        </Modal.Footer>
      </Form>
    </Modal>
  )
}

export default AddMembershipModal
