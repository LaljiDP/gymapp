import * as React from 'react'
import { RouteComponentProps } from 'react-router'
import { toast } from 'react-toastify'
import { fetchUserInfo } from '../../request/dashboard.request'
import { Card, Badge } from 'react-bootstrap'
import './dashboard.scss'
import { fetchUserCount, fetchMemberCount } from '../../store/actions/dashboard.action'
import { useSelector } from 'react-redux'
import { StoreI } from '../../store/store.types'

interface DashboardPropsI extends RouteComponentProps {
  name?: string
}

const DashboardContainer: React.FC<DashboardPropsI> = (): React.ReactElement => {
  const userCount = useSelector((state: StoreI) => state.dashboard.users)
  const membersCount = useSelector((state: StoreI) => state.dashboard.members)

  React.useEffect(() => {
    fetchUserInfo()
    fetchUserCount(msg => {
      toast.error(msg, {
        position: toast.POSITION.TOP_CENTER,
      })
    })
    fetchMemberCount(msg => {
      toast.error(msg, {
        position: toast.POSITION.TOP_CENTER,
      })
    })
  }, [])

  return (
    <div>
      <div className='d-flex flex-wrap justify-content-between align-items-center p-2'>
        <Card className='p-3 m-1 font-weight-bold w-100'>
          <div className='d-flex justify-content-between align-items-center'>
            <div>Total Members </div>
            <Badge variant='secondary'>
              <h3>{membersCount} </h3>
            </Badge>
          </div>
        </Card>
        <Card className='p-3 m-1 font-weight-bold w-100'>
          <div className='d-flex justify-content-between align-items-center'>
            <div>Total Users </div>
            <Badge variant='secondary'>
              <h3>{userCount} </h3>
            </Badge>
          </div>
        </Card>
      </div>
    </div>
  )
}

export default DashboardContainer
