"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _express = _interopRequireDefault(require("express"));

var _passport = _interopRequireDefault(require("passport"));

var _members = _interopRequireDefault(require("../models/members.schema"));

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { (0, _defineProperty2.default)(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

var router = _express.default.Router();

router.post('/', _passport.default.authenticate('jwt', {
  session: false
}),
/*#__PURE__*/
function () {
  var _ref = (0, _asyncToGenerator2.default)(
  /*#__PURE__*/
  _regenerator.default.mark(function _callee(req, res) {
    var member;
    return _regenerator.default.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            if (!req.user) res.status(401).json({
              message: 'Unauthorized'
            });
            _context.prev = 1;
            _context.next = 4;
            return new _members.default(_objectSpread({}, req.body)).save();

          case 4:
            member = _context.sent;

            if (member) {
              res.send(member);
            } else {
              res.status(501).json({
                message: 'Something wrong..'
              });
            }

            _context.next = 11;
            break;

          case 8:
            _context.prev = 8;
            _context.t0 = _context["catch"](1);
            res.status(501).json({
              message: _context.t0.toString()
            });

          case 11:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, null, [[1, 8]]);
  }));

  return function (_x, _x2) {
    return _ref.apply(this, arguments);
  };
}());
router.get('/:id/single', _passport.default.authenticate('jwt', {
  session: false
}),
/*#__PURE__*/
function () {
  var _ref2 = (0, _asyncToGenerator2.default)(
  /*#__PURE__*/
  _regenerator.default.mark(function _callee2(req, res) {
    var id, member;
    return _regenerator.default.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            if (!req.user) res.status(401).json({
              message: 'Unauthorized'
            });
            id = req.params.id;
            _context2.prev = 2;
            _context2.next = 5;
            return _members.default.findById(id).populate('trainer');

          case 5:
            member = _context2.sent;
            res.send(member);
            _context2.next = 12;
            break;

          case 9:
            _context2.prev = 9;
            _context2.t0 = _context2["catch"](2);
            res.status(501).json({
              message: 'Error while fetching member information..',
              trace: _context2.t0.toString()
            });

          case 12:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2, null, [[2, 9]]);
  }));

  return function (_x3, _x4) {
    return _ref2.apply(this, arguments);
  };
}());
router.get('/total', _passport.default.authenticate('jwt', {
  session: false
}),
/*#__PURE__*/
function () {
  var _ref3 = (0, _asyncToGenerator2.default)(
  /*#__PURE__*/
  _regenerator.default.mark(function _callee3(req, res) {
    var total;
    return _regenerator.default.wrap(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            if (!req.user) res.status(401).json({
              message: 'Unauthorized'
            });
            _context3.prev = 1;
            _context3.next = 4;
            return _members.default.count();

          case 4:
            total = _context3.sent;
            res.send({
              total: total
            });
            _context3.next = 11;
            break;

          case 8:
            _context3.prev = 8;
            _context3.t0 = _context3["catch"](1);
            res.status(501).json({
              message: _context3.t0.toString()
            });

          case 11:
          case "end":
            return _context3.stop();
        }
      }
    }, _callee3, null, [[1, 8]]);
  }));

  return function (_x5, _x6) {
    return _ref3.apply(this, arguments);
  };
}());
router.put('/:id', _passport.default.authenticate('jwt', {
  session: false
}),
/*#__PURE__*/
function () {
  var _ref4 = (0, _asyncToGenerator2.default)(
  /*#__PURE__*/
  _regenerator.default.mark(function _callee4(req, res) {
    var id, conditions, update, options, member, result;
    return _regenerator.default.wrap(function _callee4$(_context4) {
      while (1) {
        switch (_context4.prev = _context4.next) {
          case 0:
            if (!req.user) res.status(401).json({
              message: 'Unauthorized'
            });
            id = req.params.id;
            conditions = {
              _id: id
            }, update = _objectSpread({}, req.body), options = {
              multi: true
            };
            _context4.prev = 3;
            _context4.next = 6;
            return _members.default.update(conditions, update, options);

          case 6:
            member = _context4.sent;

            if (!(member.ok === 1)) {
              _context4.next = 14;
              break;
            }

            _context4.next = 10;
            return _members.default.findById(id).populate('trainer');

          case 10:
            result = _context4.sent;
            res.send(result);
            _context4.next = 15;
            break;

          case 14:
            console.log('member', member);

          case 15:
            _context4.next = 21;
            break;

          case 17:
            _context4.prev = 17;
            _context4.t0 = _context4["catch"](3);
            console.log(_context4.t0);
            res.status(501).json({
              message: 'Error while updating Member Docs.',
              trace: _context4.t0.toString()
            });

          case 21:
          case "end":
            return _context4.stop();
        }
      }
    }, _callee4, null, [[3, 17]]);
  }));

  return function (_x7, _x8) {
    return _ref4.apply(this, arguments);
  };
}());
router.delete('/:id', _passport.default.authenticate('jwt', {
  session: false
}),
/*#__PURE__*/
function () {
  var _ref5 = (0, _asyncToGenerator2.default)(
  /*#__PURE__*/
  _regenerator.default.mark(function _callee5(req, res) {
    var id, member;
    return _regenerator.default.wrap(function _callee5$(_context5) {
      while (1) {
        switch (_context5.prev = _context5.next) {
          case 0:
            if (!req.user) res.status(401).json({
              message: 'Unauthorized'
            });
            id = req.params.id;
            _context5.prev = 2;
            _context5.next = 5;
            return _members.default.deleteOne({
              _id: id
            });

          case 5:
            member = _context5.sent;
            res.send(member);
            _context5.next = 12;
            break;

          case 9:
            _context5.prev = 9;
            _context5.t0 = _context5["catch"](2);
            res.status(501).json({
              message: 'Somthing wrong',
              trace: _context5.t0.toString()
            });

          case 12:
          case "end":
            return _context5.stop();
        }
      }
    }, _callee5, null, [[2, 9]]);
  }));

  return function (_x9, _x10) {
    return _ref5.apply(this, arguments);
  };
}());
router.get('/:page', _passport.default.authenticate('jwt', {
  session: false
}),
/*#__PURE__*/
function () {
  var _ref6 = (0, _asyncToGenerator2.default)(
  /*#__PURE__*/
  _regenerator.default.mark(function _callee6(req, res) {
    var page, members, totalRecord;
    return _regenerator.default.wrap(function _callee6$(_context6) {
      while (1) {
        switch (_context6.prev = _context6.next) {
          case 0:
            if (!req.user) res.status(401).json({
              message: 'Unauthorized'
            });
            page = req.params.page;
            console.log('page', page);
            if (!page) res.status(422).json({
              message: 'Bad Request ! parameter required'
            });
            _context6.prev = 4;
            _context6.next = 7;
            return _members.default.find({}, null, {
              skip: page * 10,
              limit: 10
            }).sort({
              createdAt: 'desc'
            });

          case 7:
            members = _context6.sent;
            _context6.next = 10;
            return _members.default.count();

          case 10:
            totalRecord = _context6.sent;
            res.send({
              members: members,
              total: totalRecord
            });
            _context6.next = 17;
            break;

          case 14:
            _context6.prev = 14;
            _context6.t0 = _context6["catch"](4);
            res.status(501).json({
              message: 'Error while fetching members.',
              stacktrace: _context6.t0.toString()
            });

          case 17:
          case "end":
            return _context6.stop();
        }
      }
    }, _callee6, null, [[4, 14]]);
  }));

  return function (_x11, _x12) {
    return _ref6.apply(this, arguments);
  };
}());
var _default = router;
exports.default = _default;
//# sourceMappingURL=members.route.js.map