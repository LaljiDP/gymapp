import * as React from 'react'
import { FormControl, Form, Button } from 'react-bootstrap'
import './settings.scss'

const Settings: React.FC = (): React.ReactElement => {
  return (
    <div className='settings-container'>
      <div className='header-fixed'>
        <Button className='float-right' size='sm'>
          Save
        </Button>
      </div>
      <div className='settings-body'>
        <Form.Group>
          <Form.Label className='m-0'>members list view</Form.Label>
          <FormControl as='select'>
            <option value='default'>default</option>
            <option value='alphabetical'>alphabetical</option>
          </FormControl>
        </Form.Group>
      </div>
    </div>
  )
}

export default Settings
