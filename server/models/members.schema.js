import mongoose from 'mongoose'
const Schema = mongoose.Schema

const memberSchema = new Schema({
  name: { type: String, required: true },
  dob: { type: String, required: true },
  address: { type: String, required: true },
  weight: Number,
  height: String,
  email: String,
  blood_group: String,
  city: String,
  state: String,
  profile_pic: String,
  country: String,
  mobile_no: String,
  emergency_mobile_no: String,
  schedule: [
    {
      day: String,
      excercise: String,
      default: [],
    },
  ],
  trainer: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
  },
  memberships: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Membership'
  }],
  excercise: { type: String, required: true },
  dateOfJoining: { type: Date, required: true, default: Date.now },
  isDeleted: { tpye: Boolean, default: false },
  createdAt: { type: Date, default: Date.now },
  updatedAt: { type: Date, default: Date.now },
})

export default mongoose.model('Member', memberSchema)
