import mongoose from 'mongoose'
const Schema = mongoose.Schema

const settingSchema = new Schema({
  key: { type: String, required: true, unique: true },
  label: { type: String, required: true },
  value: { type: String, required: true, default: 'default' },
})

export default mongoose.model('setting', settingSchema)
