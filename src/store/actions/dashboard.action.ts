import request from '../../request'
import { dispatch } from '../index'
import { storeUserCount, storeMemberCount } from '../reducers/dashboard.reducer'

export const fetchUserCount = async (callback: (err: string) => void) => {
  try {
    const count = await request('/api/user/total')
    dispatch(storeUserCount(count.data.total))
  } catch (err) {
    callback('Something wrong while fetching users count!')
  }
}

export const fetchMemberCount = async (callback: (err: string) => void) => {
  try {
    const count = await request('/api/member/total')
    dispatch(storeMemberCount(count.data.total))
  } catch (err) {
    callback('error while fetching members count!')
  }
}
