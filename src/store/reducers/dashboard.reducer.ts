import { createSlice } from '@reduxjs/toolkit'
import { ActionI } from '../store.types'

const initialState: any = {
  users: 0,
  loading: false,
  members: 0,
}

export const dashboardSlice = createSlice({
  name: 'dashboard',
  initialState: initialState,
  reducers: {
    storeUserCount: (state, action: ActionI) => {
      state.users = action.payload
    },
    storeMemberCount: (state, action: ActionI) => {
      state.members = action.payload
    },
    loading: (state, action: any) => {
      state.loading = action.payload
    },
  },
})

export const { storeMemberCount, storeUserCount, loading } = dashboardSlice.actions

export const dashboardReducer = dashboardSlice.reducer
