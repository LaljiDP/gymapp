import * as React from 'react'
import { Button } from 'react-bootstrap'
import './common.scss'

interface TogglePlusButtonPropsI {
    actionFC: () => void
}

const TogglePlusButton: React.FC<TogglePlusButtonPropsI> = (props) => {
  return (
    <div className='members-toggleBtn'>
      <Button onClick={props.actionFC} className='toggle-btn'>
        <i className='fa fa-plus' />
      </Button>
    </div>
  )
}

export default TogglePlusButton
