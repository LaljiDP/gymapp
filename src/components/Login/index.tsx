import * as React from 'react'
import * as yup from 'yup'
import { toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import { withRouter, useHistory } from 'react-router'
import { Container, FormControl, Button, Spinner, Form, FormGroup } from 'react-bootstrap'
import { login } from '../../request/auth.request'
import { AxiosResponse, AxiosError } from 'axios'
import './login.scss'
import { useFormik, FormikValues } from 'formik'

const loginSchema = yup.object().shape({
  username: yup.string().required('Username is required'),
  password: yup.string().required('Password required'),
})

const Login: React.FC = (): React.ReactElement => {
  const history = useHistory()
  const [loading, setLoading] = React.useState(false)

  const gotoRegister = () => {
    history.push('/register')
  }

  const handleLogin = (payload: FormikValues) => {
    setLoading(true)
    login(payload)
      .then((res: AxiosResponse) => {
        if (res?.data?.token) {
          localStorage.setItem('token', res.data.token)
          history.push('/admin/dashboard')
        }
        setLoading(false)
      })
      .catch((err: AxiosError) => {
        console.log('err', err)
        toast.error('Invalid Credentials.', {
          position: toast.POSITION.TOP_CENTER,
        })
        setLoading(false)
      })
  }

  const { handleChange, handleSubmit, values, touched, errors } = useFormik({
    initialValues: { username: '', password: '' },
    validationSchema: loginSchema,
    onSubmit: handleLogin,
  })

  return (
    <Container className='container-fluid vh-100 d-flex align-items-center justify-content-center'>
      <div className='width-login-form'>
        <div className='text-center text-primary'>
          <h3> App </h3>
        </div>
        <Form onSubmit={handleSubmit}>
          <div className='mt-4'>
            <div className='mb-3'>
              <div>
                <FormGroup>
                  <FormControl
                    placeholder='Username'
                    value={values.username}
                    name='username'
                    isValid={touched.username && !errors.username}
                    isInvalid={touched.username && errors.username ? true : false}
                    onChange={handleChange}
                    aria-label='Username'
                    aria-describedby='basic-addon1'
                  />
                  <Form.Text className='text-danger'>{errors.username}</Form.Text>
                </FormGroup>
              </div>
            </div>
            <div className='mb-3'>
              <div>
                <FormGroup>
                  <FormControl
                    placeholder='Password'
                    aria-label='Password'
                    name='password'
                    isValid={touched.password && !errors.password}
                    isInvalid={touched.password && errors.password ? true : false}
                    onChange={handleChange}
                    value={values.password}
                    type='password'
                    aria-describedby='basic-addon1'
                  />
                  <Form.Text className='text-danger'>{errors.password}</Form.Text>
                </FormGroup>
              </div>
            </div>
            <div className='mb-3'>
              <div>
                <Button disabled={loading} className='w-100' type='submit'>
                  Login
                  {loading && <Spinner className='ml-2' animation='border' size='sm' />}
                </Button>
              </div>
              <div>
                <Button onClick={gotoRegister} className='w-100 mt-2 bg-secondary'>
                  Register
                </Button>
              </div>
            </div>
          </div>
        </Form>
      </div>
    </Container>
  )
}

export default withRouter(Login)
