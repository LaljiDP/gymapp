"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _mongoose = _interopRequireDefault(require("mongoose"));

var Schema = _mongoose.default.Schema;
var settingSchema = new Schema({
  key: {
    type: String,
    required: true,
    unique: true
  },
  label: {
    type: String,
    required: true
  },
  value: {
    type: String,
    required: true,
    default: 'default'
  }
});

var _default = _mongoose.default.model('setting', settingSchema);

exports.default = _default;
//# sourceMappingURL=setting.schema.js.map