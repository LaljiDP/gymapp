import mongoose from 'mongoose'
const Schema = mongoose.Schema

const userSchema = new Schema({
  name: String,
  dob: Date,
  userName: String,
  email: String,
  password: String,
  mobile_no: String,
  members: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Member',
    },
  ],
  lastLoggedIn: { type: Date, default: Date.now },
  createdAt: { type: Date, default: Date.now },
  updatedAt: { type: Date, default: Date.now },
  role: { type: String, required: true },
})

export default mongoose.model('User', userSchema)
