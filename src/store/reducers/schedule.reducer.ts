import { createSlice } from '@reduxjs/toolkit'
import { ScheduleReducerI } from '../store.types'

interface ActionI {
  type: string
  payload: any
}

const initialState: ScheduleReducerI = {
  data: [],
  loading: false,
  page: 0,
  total: 0,
}

const scheduleSlice = createSlice({
  name: 'schedule',
  initialState,
  reducers: {
    storeSchedule: (state, action: ActionI) => {
      state.data = action.payload
    },
    loading: (state, action: ActionI) => {
      state.loading = action.payload
    },
    addSchedule: (state, action: ActionI) => {
      let ss = state.data.slice()
      ss.unshift(action.payload)
      state.data = ss
    },
  },
})

export const { storeSchedule, loading, addSchedule } = scheduleSlice.actions

export const scheduleReducer = scheduleSlice.reducer
