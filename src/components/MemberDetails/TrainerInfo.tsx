import * as React from 'react'
import { Transition } from 'react-transition-group'
import { UserI } from '../Users/users.types'
import UserIcom from '../../assets/user.svg'

interface TrianerInfoPropsI {
  expand: boolean
  data?: UserI
  openTainerModal: () => void
  handleCollapse: (data: any) => void
}

const TrainerInfo: React.FC<TrianerInfoPropsI> = ({
  expand,
  data,
  openTainerModal,
  handleCollapse,
}): React.ReactElement => {
  const isUpdate = data?._id ? true : false

  return (
    <div className='member-schedule'>
      <div className='alert font-weight-bold alert-primary'>
        <div className='d-flex justify-content-between'>
          <div>Trianer</div>
          <div className='schedule-icons'>
            {isUpdate ? (
              <i onClick={openTainerModal} className='fa fa-edit' />
            ) : (
              <i onClick={openTainerModal} className='fa fa-plus' />
            )}
            <i
              onClick={() => handleCollapse({ trainer: !expand })}
              className={`fa ${expand ? 'fa-angle-down' : 'fa-angle-right'} pl-4`}
            />
          </div>
        </div>
      </div>
      <Transition in={expand} timeout={1000}>
        {state => (
          <div className={`schedule-container ${expand ? 'd-block' : 'd-none'}`}>
            <div className='p-3'>
              {data?._id ? (
                <div className='d-flex justify-content-around align-items-center'>
                  <div>
                    <img src={data?.profile_pic || UserIcom} alt='img' />
                  </div>
                  <div className='font-weight-bold'>{data?.name}</div>
                </div>
              ) : (
                <div className='text-danger text-center'> Need to assign trianer. </div>
              )}
            </div>
          </div>
        )}
      </Transition>
    </div>
  )
}

export default TrainerInfo
