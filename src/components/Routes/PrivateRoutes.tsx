import * as React from 'react'
import { Route, Redirect } from 'react-router-dom'
import { withRouter, RouteComponentProps } from 'react-router'
import { fetchUserInfo } from '../../request/dashboard.request'
import Loading from '../Common/Loading'

interface PrivateRoutesPropsI extends RouteComponentProps {
  component: any
  path: string
}

const PrivateRoutes: React.FC<PrivateRoutesPropsI> = ({ component: Component, ...rest }): React.ReactElement => {
  const [loading, setLoading] = React.useState(true)
  const [isUserValid, setUserValid] = React.useState(false)

  React.useEffect(() => {
    console.log('renderess...')
    console.log('componentDidMount')
    async function checkUser() {
      const isValid: boolean = await isLoggedIn()
      if (isValid) {
        setUserValid(true)
        setLoading(false)
      } else {
        setUserValid(false)
        setLoading(false)
      }
    }
    checkUser()
  })

  const renderRoute = () => {
    if (loading) {
      return <Loading centered={true} />
    } else if (!loading && isUserValid) {
      return <Route {...rest} component={() => <Component {...rest} />} />
    } else {
      return <Redirect to='/' />
    }
  }

  return renderRoute()
}

const isLoggedIn = async () => {
  try {
    const user = await fetchUserInfo()
    if (user?.data?.user?._id) {
      return true
    }
    return false
  } catch (err) {
    console.log('false...catch')
    return false
  }
}

export default withRouter(PrivateRoutes)
